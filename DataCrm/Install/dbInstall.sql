USE gebedenboek;

--  Voor eerste uit als root : 64Mb
--  SET GLOBAL max_allowed_packet = 1024*1024*64;
--  Verifieer met :
--  Select @@global.max_allowed_packet;

--
-- Table structure for table payer
--

DROP TABLE IF EXISTS appsettings;
CREATE TABLE appsettings (
	ident      varchar(25) not null,
	hostname   varchar(100) not null,
	port       int(11) NOT NULL,
	username   varchar(100) not null,
	wachtwoord varchar(100) not null,
  sendername varchar(100) not null,
  sender     varchar(100) not null,
	PRIMARY KEY (ident)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS bezoeker;
CREATE TABLE bezoeker (
		id bigint(20) NOT NULL AUTO_INCREMENT,
		email varchar(100) DEFAULT NULL,
		geslacht varchar(10) NOT NULL,
		voornaam varchar(25) NOT NULL,
		achternaam varchar(50) NOT NULL,
		geboortedatum datetime NOT NULL,
		postcode varchar(8) DEFAULT NULL,
		woonplaats varchar(25) DEFAULT NULL,
		nummer varchar(10) DEFAULT NULL,
		locatieid bigint(20) NOT NULL DEFAULT '1',
		straatnaam varchar(60) DEFAULT NULL,
		telefoon_dag varchar(20) DEFAULT NULL,
		telefoon_nacht varchar(20) DEFAULT NULL,
		hoecontactkerk varchar(200) NOT NULL,
		imageid bigint(20) DEFAULT NULL,
		toestemming BOOLEAN NOT NULL DEFAULT FALSE,
		PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS filesdata;
CREATE TABLE filesdata (
	id bigint(20) NOT NULL AUTO_INCREMENT,
	filebytes mediumblob not null,
	extension varchar(5) not null,
	filetype varchar(25) not null,
	title varchar(50) not NULL,
	createdate datetime NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS messageimage;
CREATE TABLE messageimage (
	id bigint(20) NOT NULL AUTO_INCREMENT,
	filebytes mediumblob not null,
	extension varchar(5) not null,
	filetype varchar(25) not null,
	title varchar(50) not NULL,
	createdate datetime NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS messages;
CREATE TABLE messages (
	id bigint(20) NOT NULL AUTO_INCREMENT,
	title varchar(50) not NULL,
	messagebody longtext not null,
	createdate datetime NOT NULL,
	PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS webuser;
CREATE TABLE webuser (
  userid varchar(15) NOT NULL,
  wachtwoord varchar(50) NOT NULL,
  voornaam varchar(25) NOT NULL,
  achternaam varchar(50) NOT NULL,
  groupnaam varchar(15) DEFAULT NULL,
  PRIMARY KEY (userid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table locaties
--

DROP TABLE IF EXISTS locaties;

CREATE TABLE locaties (
		id bigint(20) NOT NULL AUTO_INCREMENT,
		straatnaam varchar(60) NOT NULL,
		nummer int(11) NOT NULL,
		toevoeging varchar(4) DEFAULT NULL,
		woonplaats varchar(25) NOT NULL,
		postcode varchar(8) DEFAULT NULL,
		telefoon varchar(20) DEFAULT NULL,
		ukgrcode int(11) NOT NULL,
		PRIMARY KEY (id),
		UNIQUE KEY ukgrcode_UNIQUE (ukgrcode)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE messagestosend (
    receiverid bigint(20) NOT NULL ,
    messageid bigint(20) NOT NULL ,
		email varchar(100) NOT NULL,
    title varchar(50) NOT NULL,
		slottekst varchar(200) NOT NULL,
		koptekst varchar(200) NOT NULL,
    fullname varchar(200) NOT NULL,
    datetosend  datetime NOT NULL,
    completed BOOLEAN default false,
    PRIMARY KEY (receiverid,messageid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO appsettings VALUES ('quartzMail','smtp-mail.outlook.com',587,'appArwLutchman@outlook.com','Deepay#18','Amrit Lutchman','appArwLutchman@outlook.com');


INSERT INTO locaties
(straatnaam,nummer,toevoeging,woonplaats,postcode,telefoon,ukgrcode)
VALUES
('Fruitweg',4,NULL,'Den Haag','2525KH','0703888442',1),
('Diergaardesingel',70,NULL,'Rotterdam','3014AE',NULL,2),
('Westersingel',42,NULL,'Rotterdam','3014GT',NULL,3),
('Borneostraat',40,NULL,'Amsterdam Oost','1094CL',NULL,4),
('Jan Evertsenstraat',18,NULL,'Amsterdam West','1056EC',NULL,5),
('Piet Bakkerhove',7,NULL,'Zoetermeer','2717ZA',NULL,6),
('Grote Markt',27,NULL,'Almere','1315JA',NULL,7),
('Oudegracht',60,NULL,'Utrecht','3511AS',NULL,8),
('Boschdijk',258,NULL,'Eindhoven','5612HJ',NULL,9);

--20200614
--ALTER TABLE bezoeker ADD COLUMN toestemming BOOLEAN NOT NULL DEFAULT FALSE;