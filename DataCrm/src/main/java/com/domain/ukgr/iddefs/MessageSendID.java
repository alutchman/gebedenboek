package com.domain.ukgr.iddefs;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class MessageSendID implements Serializable {
    private static final long serialVersionUID = -191202173461211862L;
    private Long receiverid;
    private Long messageid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageSendID that = (MessageSendID) o;
        return getReceiverid().equals(that.getReceiverid()) &&
                getMessageid().equals(that.getMessageid());
    }

    @Override
    public int hashCode() {
        int result = getMessageid().hashCode();
        result = 31 * result + getReceiverid().hashCode();
        return result;
    }
}
