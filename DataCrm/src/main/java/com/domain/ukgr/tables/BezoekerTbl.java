package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@ToString
@Data
@EqualsAndHashCode(callSuper = false, exclude={"hoecontactkerk", "telefoonNacht","telefoonDag","straatnaam","woonplaats"})
@Entity
@Table(name = "bezoeker")
public class BezoekerTbl extends BaseEntityId {
    private static final long serialVersionUID = -8839875682989488388L;

    @Column(name = "email")
    private String email;

    @Column(name = "geslacht")
    private String geslacht;

    @Column(name = "voornaam")
    private String voornaam;

    @Column(name = "achternaam")
    private String achternaam;

    @Column(name = "geboortedatum")
    private Timestamp geboortedatum;

    @Column(name = "postcode")
    private String postcode;

    @Column(name = "woonplaats")
    private String woonplaats;

    @Column(name = "nummer")
    private String nummer;

    @Column(name = "locatieid")
    protected Long locatieID;

    @Column(name = "straatnaam")
    private String straatnaam;

    @Column(name = "telefoon_dag")
    private String telefoonDag;

    @Column(name = "telefoon_nacht")
    private String telefoonNacht;

    @Column(name = "hoecontactkerk")
    private String hoecontactkerk;

    @Column(name = "imageid")
    private Long imageid;

    @Column(name = "toestemming")
    private Boolean toestemming;
}
