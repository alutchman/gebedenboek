package com.domain.ukgr.tables;


import com.domain.ukgr.entity.BaseEntity;
import com.domain.ukgr.iddefs.MessageSendID;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@ToString
@Getter
@Setter
@IdClass(MessageSendID.class)
@Entity
@Table(name = "messagestosend")
public class MessagesToSend extends BaseEntity {
    private static final long serialVersionUID = -8175177916653382971L;

    @Id
    private Long receiverid;
    @Id
    private Long messageid;

    private String email;

    private String title;

    private String koptekst;

    private String slottekst;

    private String fullname ;

    private Timestamp datetosend  ;

    private boolean completed;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessagesToSend that = (MessagesToSend) o;
        return getReceiverid().equals(that.getReceiverid()) &&
                getMessageid().equals(that.getMessageid());
    }

    @Override
    public int hashCode() {
        int result = getMessageid().hashCode();
        result = 31 * result + getReceiverid().hashCode();
        return result;
    }
}
