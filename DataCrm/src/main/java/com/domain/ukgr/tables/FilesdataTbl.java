package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@ToString
@Data
@EqualsAndHashCode(callSuper = true, exclude={"filebytes", "createdate"})
@Entity
@Table(name = "filesdata")
public class FilesdataTbl extends BaseEntityId {
    private static final long serialVersionUID = 2653116007281948049L;

    @Column(name = "filebytes"  , nullable=false, columnDefinition="mediumblob")
    private byte[] filebytes;

    @Column(name = "extension")
    private String extension;

    @Column(name = "title")
    private String title;

    @Column(name = "createdate")
    private Timestamp createdate;

    @Column(name = "filetype")
    private String filetype;

}
