package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@ToString
@Data
@EqualsAndHashCode(callSuper = false)
@Entity
@Table(name = "appsettings")
public class AppsettingsTbl extends BaseEntity  {

    private static final long serialVersionUID = 8260489856977616455L;

    @Id
    @Column( name = "ident",nullable = false, length = 25, unique = true)
    private String ident;

    @Column( name = "hostname",nullable = false, length = 100)
    private String hostname;

    @Column(name = "port")
    private int port;

    @Column( name = "username",nullable = false, length = 100)
    private String username;

    @Column( name = "wachtwoord",nullable = false, length = 100)
    private String wachtwoord;

    @Column( name = "sendername",nullable = false, length = 100)
    private String sendername;


    @Column( name = "sender",nullable = false, length = 100)
    private String sender;
}