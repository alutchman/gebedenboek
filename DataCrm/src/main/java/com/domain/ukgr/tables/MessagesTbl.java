package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@ToString
@Data
@EqualsAndHashCode(callSuper = true, exclude={"messagebody", "createdate"})
@Entity
@Table(name = "messages")
public class MessagesTbl extends BaseEntityId {
    private static final long serialVersionUID = 4516876600366105541L;

    @Column(name = "title")
    private String title;


    @Column(name = "messagebody" , nullable=false, columnDefinition="longtext")
    private String messagebody;

    @Column(name = "createdate")
    private Timestamp createdate;

}
