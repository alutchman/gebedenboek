package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@ToString
@Data
@EqualsAndHashCode(callSuper = false, exclude={"wachtwoord"})
@Entity
@Table(name = "webuser")
public class Webuser extends BaseEntity {
  private static final long serialVersionUID = 1128106384751953049L;
  @Id
  @Column( name = "userid",nullable = false, length = 15, unique = true)
  private String userid;

  @Column( name = "wachtwoord",nullable = false, length = 50)
  private String wachtwoord;

  @Column( name = "voornaam",nullable = false, length = 25)
  private String voornaam;

  @Column( name = "achternaam",nullable = false, length = 50)
  private String achternaam;

  @Column( name = "groupnaam",nullable = false, length = 15)
  private String groupnaam;



}
