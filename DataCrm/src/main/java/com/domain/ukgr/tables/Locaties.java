package com.domain.ukgr.tables;

import com.domain.ukgr.entity.BaseEntityId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@ToString
@Data
@EqualsAndHashCode(callSuper = true, exclude={"toevoeging", "telefoon"})
@Entity
@Table(
        name = "locaties",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"ukgrcode"})}
        )
public class Locaties extends BaseEntityId {
  private static final long serialVersionUID = -8648797440927902796L;

  @Column( name = "straatnaam",nullable = false, length = 60)
  private String straatnaam;

  @Column( name = "nummer",nullable = false)
  private Integer nummer;

  @Column( name = "toevoeging")
  private String toevoeging;

  @Column( name = "woonplaats",nullable = false, length = 25)
  private String woonplaats;

  @Column( name = "postcode",nullable = false, length = 8)
  private String postcode;

  @Column( name = "telefoon", length = 20)
  private String telefoon;

  @Column( name = "ukgrcode",nullable = false)
  private Integer ukgrcode;


}
