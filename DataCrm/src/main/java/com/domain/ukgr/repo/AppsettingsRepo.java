package com.domain.ukgr.repo;

import com.domain.ukgr.tables.AppsettingsTbl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppsettingsRepo extends JpaRepository<AppsettingsTbl, String> {
}
