package com.domain.ukgr.repo;

import com.domain.ukgr.iddefs.MessageSendID;
import com.domain.ukgr.tables.MessagesToSend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface MessagesToSendRepo extends JpaRepository<MessagesToSend, MessageSendID> {

    @Query("SELECT t FROM MessagesToSend t where t.messageid =  ?1 AND t.datetosend <= ?2 ")
    Optional<List<MessagesToSend>> getMessageToSendList(Long messageid, Timestamp fromDate);
}
