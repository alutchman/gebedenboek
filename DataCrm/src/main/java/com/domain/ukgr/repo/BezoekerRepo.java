package com.domain.ukgr.repo;

import com.domain.ukgr.tables.BezoekerTbl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BezoekerRepo extends JpaRepository<BezoekerTbl, Long> {


    @Query("SELECT t FROM BezoekerTbl t where email=?1 ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    Optional<List<BezoekerTbl>>  findByEmail(String email);

    @Query("SELECT t FROM BezoekerTbl t where email=?1 AND voornaam=?2 ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    Optional<List<BezoekerTbl>>  findByEmailAndFirstName(String email, String voornaam);

    @Query("SELECT t FROM BezoekerTbl t where achternaam like ?1 AND voornaam like ?2 ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    Optional<List<BezoekerTbl>>  findByNames(String achternaam, String voornaam);

    @Query("SELECT t FROM BezoekerTbl t where achternaam like ?1  ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    Optional<List<BezoekerTbl>> findByLastNameLike(String achternaam);

    @Query("SELECT t FROM BezoekerTbl t where email=?1 AND voornaam=?2  ORDER BY UPPER(t.achternaam) ASC, UPPER(t.voornaam) ASC")
    Optional<List<BezoekerTbl>> findByLastNameAndEmail(String email, String achternaam);
}
