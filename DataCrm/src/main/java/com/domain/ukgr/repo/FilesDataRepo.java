package com.domain.ukgr.repo;

import com.domain.ukgr.tables.FilesdataTbl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilesDataRepo extends JpaRepository<FilesdataTbl, Long> {
}
