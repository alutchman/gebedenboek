package com.domain.ukgr.repo;

import com.domain.ukgr.tables.MessageImageTbl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageImageRepo extends JpaRepository<MessageImageTbl, Long> {
}
