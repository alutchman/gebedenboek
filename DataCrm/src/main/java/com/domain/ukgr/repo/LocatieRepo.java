package com.domain.ukgr.repo;

import com.domain.ukgr.tables.Locaties;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LocatieRepo  extends JpaRepository<Locaties, Long> {
    @Query("SELECT t FROM Locaties t ORDER BY UPPER(t.woonplaats) ASC, UPPER(t.straatnaam) ASC")
    Optional<List<Locaties>> findLocationsSorted();

    @Query("SELECT t FROM Locaties t where ukgrcode = ?1 AND id <> ?2 ")
    Optional<Locaties> findLocatiesByUkgrCode(Integer ukgrcode, Long locatieId);
}
