package com.domain.ukgr.repo;

import com.domain.ukgr.tables.MessagesTbl;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface MessagesRepo  extends JpaRepository<MessagesTbl, Long> {

    @Query("SELECT t FROM MessagesTbl t where t.title like ?1 AND t.createdate >= ?2 ")
    Optional<List<MessagesTbl>> searchUser(String title, Timestamp fromDate);

    @Query("SELECT t FROM MessagesTbl t where t.title like ?1 ")
    Optional<List<MessagesTbl>> searchUser(String title);
}
