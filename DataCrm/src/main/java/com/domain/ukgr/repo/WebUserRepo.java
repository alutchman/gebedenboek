package com.domain.ukgr.repo;

import com.domain.ukgr.tables.Webuser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface WebUserRepo extends JpaRepository<Webuser, String> {

    @Query("SELECT t FROM Webuser t where userid=?1 AND wachtwoord=?2")
    Optional<Webuser> finduser(String userid, String wachtwoord);

    @Query("SELECT t FROM Webuser t where userid=?1")
    Optional<Webuser> finduser(String userid);


    @Query("SELECT t FROM Webuser t where voornaam like ?1 AND achternaam like ?2 AND userid <> ?3")
    Optional<List<Webuser>> searchUser(String voornaam, String achternaam, String userIdexclude);
}
