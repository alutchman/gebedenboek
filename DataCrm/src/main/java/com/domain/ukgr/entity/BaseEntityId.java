package com.domain.ukgr.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@ToString
@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
public class BaseEntityId extends BaseEntity{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    protected Long id;


}
