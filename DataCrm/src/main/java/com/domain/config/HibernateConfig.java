package com.domain.config;


import com.jndi.env.JndiTemplateDataBase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@ComponentScan(
        basePackages = {"com.domain.ukgr"}
)
@EnableJpaRepositories(basePackages = {
        "com.domain.ukgr.repo"
})
@EnableTransactionManagement
public class HibernateConfig {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    protected static final String DATA_SOURCE_NAME = "jdbc/ukgrGebedenboek";


    @Bean
    public JndiTemplateDataBase fetchJndiTemplate(){
        JndiTemplateDataBase template = new JndiTemplateDataBase();
        return template;
    }


    @Bean
    public DataSource getDataSource()  {
        try {
            JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
            dsLookup.setResourceRef(true);
            return dsLookup.getDataSource(DATA_SOURCE_NAME);
        } catch (Exception e) {
            logger.error("Datasource Failure ...", e);
        }
        return null;
    }



    @Bean
    @Autowired
    public JdbcTemplate createJdbCTemplate(DataSource dataSource){
        return new JdbcTemplate(dataSource);
    }



    @Bean
    @Autowired
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource, JndiTemplateDataBase template) {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(new String[] {"com.domain.ukgr.tables" });

        JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        em.setJpaProperties(template.getEnvironment());

        return em;
    }


    @Bean
    @Autowired
    public PlatformTransactionManager transactionManager(EntityManagerFactory emf){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(emf);

        return transactionManager;
    }


}
