package com.shared.transfers;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;


@Getter
@Setter
public class ImageDataTrf implements Serializable {
    private static final long serialVersionUID = 4499204467292541616L;
    public  static final String BASE_IMG_NAME = "msgGebedDataImage";
    public  static final String BASE_CONTENT_NAME = "msgGebedDataContent";

    private Long id;

    private byte[] filebytes;

    private String extension;

    private String title;

    private Date createdate;

    private String filetype;


    @Override
    public int hashCode() {
        return getFiletype().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageDataTrf that = (ImageDataTrf) o;
        return Objects.equals(getId(), that.getId());
    }


    public String findformattedName(){
        return String.format("%s%d.%s",BASE_IMG_NAME, id,  extension);
    }

    public String findImageTag(){
        String contentType = getFiletype();
        byte[] contents = getFilebytes();

        String encoded = Base64.getEncoder().encodeToString(contents);
        String baseImage = "<img src=\"data:%s;base64,%s\"  />";
        return String.format(baseImage, contentType, encoded);
    }
}
