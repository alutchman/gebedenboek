package com.shared.transfers;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class FileDataTrf implements Serializable {

    private static final long serialVersionUID = 923695982269049218L;

    private Long id;

    private byte[] filebytes;

    private String extension;

    private String title;

    private Date createdate;

    private String filetype;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public byte[] getFilebytes() {
        return filebytes;
    }

    public void setFilebytes(byte[] filebytes) {
        this.filebytes = filebytes;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public String getFiletype() {
        return filetype;
    }

    public void setFiletype(String filetype) {
        this.filetype = filetype;
    }

    @Override
    public int hashCode() {
        return getFiletype().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileDataTrf that = (FileDataTrf) o;
        return Objects.equals(getId(), that.getId());
    }
}
