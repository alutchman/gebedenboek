package com.shared.transfers;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@Getter
@Setter
public class LocatieData implements Serializable,Cloneable {
    private static final long serialVersionUID = -165921971884966417L;

    protected Long id;

    private String straatnaam;

    private Integer nummer;

    private String toevoeging;

    private String woonplaats;

    private String postcode;

    private String telefoon;

    private Integer ukgrcode;

    @Override
    public String toString() {
        return woonplaats + ", " + straatnaam + " UKGR CODE="+ ukgrcode;
    }

    @Override
    public LocatieData clone()throws CloneNotSupportedException{
        return (LocatieData) super.clone();
    }

    public String getZipFileName(){
        String pattern = "HH-mm-ss";
        SimpleDateFormat simpleDateFormat =
                new SimpleDateFormat(pattern, new Locale("nl", "NL"));

        String postFix = simpleDateFormat.format(new Date());
        return String.format("%s_UKGRCODE_%d_%s.zip", woonplaats.replaceAll(" ",""),ukgrcode, postFix);
    }
}
