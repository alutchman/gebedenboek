package com.shared.transfers;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
public class BezoekerData implements Serializable,Cloneable{
    private static final long serialVersionUID = 2177045515940068819L;

    private Long id;

    private String email;

    private String geslacht;

    private String voornaam;

    private String achternaam;

    private Date geboortedatum;

    private String postcode;

    private String woonplaats;

    private String nummer;

    private Long locatieId;

    private String straatnaam;

    private String telefoonDag;

    private String telefoonNacht;

    private String hoecontactkerk;

    private Long imageid;

    private Boolean toestemming;


    public String getFullName(){
        return String.format("%s %s", getVoornaam(), getAchternaam() );
    }

    public String getFileNameForPdf(){
        return String.format("%s_%s_%s.pdf", getVoornaam(), getAchternaam(), getPostcode() );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BezoekerData that = (BezoekerData) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAchternaam());
    }

    @Override
    public BezoekerData clone()throws CloneNotSupportedException{
        return (BezoekerData) super.clone();
    }
}
