package com.shared.transfers;

import java.io.Serializable;

public class SearchUser implements Serializable {
    private static final long serialVersionUID = 2708222079373664362L;

    private String achternaam;
    private String voornaam;

    public String getAchternaam() {
        return achternaam;
    }

    public void setAchternaam(String achternaam) {
        this.achternaam = achternaam;
    }

    public String getVoornaam() {
        return voornaam;
    }

    public void setVoornaam(String voornaam) {
        this.voornaam = voornaam;
    }
}
