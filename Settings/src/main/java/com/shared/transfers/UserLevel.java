package com.shared.transfers;

public enum UserLevel {
    BEHEER(true,true),ADMIN(false,true),BEZOEKERS(false,false),GEEN(false, false);

    private final boolean addUsers;
    private final boolean editData;

    UserLevel(boolean addUsers, boolean editData){
        this.editData = editData;
        this.addUsers = addUsers;
    }

    public boolean isAddUsers() {
        return addUsers;
    }


    public boolean isEditData() {
        return editData;
    }
}
