package com.shared.transfers;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Data
public class MessageData implements Serializable,Cloneable {

    private static final long serialVersionUID = -4549849057953091629L;

    private Long id;

    private String title;

    private String messagebody;

    private String messagebodyTruncate;

    private Date createdate;

    private String kopTekst;

    private String slotTekst;

    private String pdfData;


    @Override
    public int hashCode() {
        return title.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageData that = (MessageData) o;
        return Objects.equals(getTitle(), that.getTitle()) &&
                Objects.equals(getMessagebody(), that.getMessagebody()) ;
    }

    @Override
    public MessageData clone() throws CloneNotSupportedException {
        return (MessageData) super.clone();
    }
}
