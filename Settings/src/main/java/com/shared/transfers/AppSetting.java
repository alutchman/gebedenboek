package com.shared.transfers;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class AppSetting {
    private String ident;

    private String hostname;

    private int port;

    private String username;

    private String wachtwoord;

    private String sendername;

    private String sender;

}
