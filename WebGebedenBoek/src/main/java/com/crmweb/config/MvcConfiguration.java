package com.crmweb.config;

import com.crmweb.jsfbean.BaseJsfBean;
import com.shared.transfers.ImageDataTrf;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import javax.servlet.ServletContext;

@Slf4j
@Configuration
@ComponentScan(
		basePackages = {"com.domain.ukgr","com.crmweb"}
)
@EnableWebMvc
public class MvcConfiguration implements WebMvcConfigurer,ApplicationListener<ContextRefreshedEvent> {
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setPrefix("/views/");
		resolver.setSuffix(".xhtml");
		return resolver;
	}


	/**
	 * This is the place to call initializations like load proxy options. This is
	 * were we land when all beans have been (re)loaded.
	 *
	 * This part is needed to tell JSF beans of the existence of Spring beans
	 *
	 */
	@Override
	public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
		String[] beanNames = contextRefreshedEvent.getApplicationContext().getBeanDefinitionNames();

		ServletContext servletContext = contextRefreshedEvent.getApplicationContext().getBean(ServletContext.class);

		for (String beanName : beanNames) {
			Object beanObject = contextRefreshedEvent.getApplicationContext().getBean(beanName);
			Class beanClass = beanObject.getClass();

			// in case class gets proxied with CGILIB
			Class wiredClass = ClassUtils.getUserClass(beanClass);

			boolean isUsable = (wiredClass.isAnnotationPresent(Service.class) ||
					wiredClass.isAnnotationPresent(Component.class)) ;

			//===only send services and components to JSF
			//==&& servletContext != null --> We demand this to be true... if not STOP
			if (isUsable ) {
				servletContext.setAttribute(wiredClass.getCanonicalName(), beanObject);
				log.info("Injection servletContext: {}", wiredClass.getCanonicalName());
			}

		}
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LoggedInInterceptor()).addPathPatterns("/**").
				excludePathPatterns("/*index.html").
				excludePathPatterns("/*login.html").
				excludePathPatterns("/*check.html").
				excludePathPatterns(String.format("/*%s*.gif", ImageDataTrf.BASE_IMG_NAME)).
				excludePathPatterns(String.format("/*%s*.png", ImageDataTrf.BASE_IMG_NAME)).
				excludePathPatterns(String.format("/*%s*.jpg", ImageDataTrf.BASE_IMG_NAME)).
				excludePathPatterns(String.format("/*%s*.jpeg", ImageDataTrf.BASE_IMG_NAME)).
				excludePathPatterns(String.format("/*%s*.html", ImageDataTrf.BASE_CONTENT_NAME));;

		log.info("Added Interceptor: {}", LoggedInInterceptor.class.getCanonicalName());
	}

}

