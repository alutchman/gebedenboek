package com.crmweb.services;


import com.crmweb.utils.TransferUtilBezoeker;
import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.repo.FilesDataRepo;
import com.domain.ukgr.repo.MessageImageRepo;
import com.domain.ukgr.tables.FilesdataTbl;
import com.domain.ukgr.tables.MessageImageTbl;
import com.shared.transfers.FileDataTrf;
import com.shared.transfers.ImageDataTrf;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.UploadedFileWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Slf4j
@Service
public class ImageService {

    @Autowired
    private MessageImageRepo messageImageRepo;


    @Autowired
    private FilesDataRepo filesDataRepo;



    @Transactional
    public FileDataTrf getImageById(Long id) {
        Optional<FilesdataTbl> result = filesDataRepo.findById(id);
        return result.isPresent() ? TransferUtilBezoeker.fromFilesdataTbl(result.get()) : null;
    }


    @Transactional
    public FilesdataTbl addNewFile(UploadedFileWrapper newBezoekersImage){
        byte[] dataImage = newBezoekersImage.getContents();
        String contentType = newBezoekersImage.getContentType();
        String filename = newBezoekersImage.getFileName();
        long size = newBezoekersImage.getSize();
        String extension = FilenameUtils.getExtension(filename);
        Timestamp timestamp =  new Timestamp(new Date().getTime());

        FilesdataTbl filesdataTbl = new FilesdataTbl();
        filesdataTbl.setCreatedate(timestamp);
        filesdataTbl.setExtension(extension);
        filesdataTbl.setFilebytes(dataImage);
        filesdataTbl.setFiletype(contentType);
        filesdataTbl.setTitle(filename);

        return  filesDataRepo.save(filesdataTbl);
    }

    public ImageDataTrf getMessageImageById(Long id) {
        Optional<MessageImageTbl> result = messageImageRepo.findById(id);
        return result.isPresent() ? TransferUtils.fromMessageImageTbl(result.get()) : null;
    }

    @Transactional
    public ImageDataTrf addNewMessageImage(UploadedFile newBezoekersImage){
        byte[] dataImage = newBezoekersImage.getContents();
        String contentType = newBezoekersImage.getContentType();
        String filename = newBezoekersImage.getFileName();
        long size = newBezoekersImage.getSize();
        String extension = FilenameUtils.getExtension(filename);

        Timestamp timestamp =  new Timestamp(new Date().getTime());

        MessageImageTbl messageImageTbl = new MessageImageTbl();
        messageImageTbl.setCreatedate(timestamp);
        messageImageTbl.setExtension(extension);
        messageImageTbl.setFilebytes(dataImage);
        messageImageTbl.setFiletype(contentType);
        messageImageTbl.setTitle(filename);

        MessageImageTbl messageImageTblNew = messageImageRepo.save(messageImageTbl);

        return  TransferUtils.fromMessageImageTbl(messageImageTblNew);
    }

}
