package com.crmweb.services;

import com.crmweb.email.HtmlMessageImageData;
import com.crmweb.email.SMTPAuthenticator;
import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.repo.AppsettingsRepo;
import com.domain.ukgr.repo.MessagesRepo;
import com.domain.ukgr.repo.MessagesToSendRepo;
import com.domain.ukgr.tables.AppsettingsTbl;
import com.domain.ukgr.tables.MessagesTbl;
import com.domain.ukgr.tables.MessagesToSend;
import com.google.common.io.Resources;
import com.shared.transfers.AppSetting;
import com.shared.transfers.BezoekerData;
import com.shared.transfers.ImageDataTrf;
import com.shared.transfers.MessageData;
import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.annotation.PostConstruct;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.transaction.Transactional;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import com.itextpdf.text.pdf.codec.Base64;

@Slf4j
@Service
public class MessagesService {
    public static final String INIT_MAIL = "quartzMail";

    @Autowired
    private MessagesRepo messagesRepo;

    @Autowired
    private AppsettingsRepo appsettingsRepo;

    @Autowired
    private ImageService imageService;

    @Autowired
    private MessagesToSendRepo messagesToSendRepo;

    private String htmlNewMessage;




    public String getHtmlNewMessage() {
        return htmlNewMessage;
    }

    @PostConstruct
    public void constrictNewMessage(){
        try {
            htmlNewMessage =  readResource("baseMsg.html", Charset.defaultCharset());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Transactional
    public List<MessageData> findMessages(String title, Date fromDate){
        if (title == null) {
            return new ArrayList<>();
        }
        Optional<List<MessagesTbl>>  tblDataList = fromDate != null ?
                messagesRepo.searchUser(title,new Timestamp(fromDate.getTime())) :
                messagesRepo.searchUser(title) ;
        List<MessagesTbl> resultTblList = null;
        if (tblDataList.isPresent()) {
            resultTblList = tblDataList.get();
            return TransferUtils.fromMessageTbl(resultTblList);
        } else {
            return new ArrayList<>();
        }
    }

    @Transactional
    public MessageData findMessageById(Long id){
        Optional<MessagesTbl> result = messagesRepo.findById(id);

        MessageData messageData = result.isPresent() ? TransferUtils.fromMessageTbl(result.get()) : null;

        if (messageData == null) {
            messageData = new MessageData();
            messageData.setMessagebody("Bestand niet gevonden !");
        }
        return messageData;
    }

    @Transactional
    public String findWithEmbeddedImgMessageById(Long id){
        MessageData messageData = findMessageById(id);

        if (messageData == null) {
            messageData = new MessageData();
            messageData.setMessagebody("Bestand niet gevonden !");
            return messageData.getMessagebody();
        } else {
            return constructEmbeddedHtml(messageData.getMessagebody());
        }
    }

    public  Map<String, ImageDataTrf> preProcessPdfData(MessageData messageData){
        Map<String, ImageDataTrf > linkedData = new HashMap<>();
        String messageBody = messageData.getMessagebody();
        Pattern pattern = Pattern.compile("<img src=\"([^\"]+)");
        Matcher matcher = pattern.matcher(messageBody);
        while (matcher.find()) {
            String group = matcher.group(1);
            String spripperPart = group.replaceAll("/GebedenBoek/msgGebedDataImage","").
                    replaceAll(".png","").
                    replaceAll(".jpg","").
                    replaceAll(".jpeg","").
                    replaceAll(".gif","");

            Long imageId = Long.valueOf(spripperPart);
            ImageDataTrf imageDataTrf = imageService.getMessageImageById(imageId);
            linkedData.put(group, imageDataTrf);
        }
        return  linkedData;
    }


    public String constructEmbeddedHtml(String messageBody ){
        Pattern pattern = Pattern.compile("<img src=\"([^\"]+)");
        Matcher matcher = pattern.matcher(messageBody);
        while (matcher.find()) {
            String group = matcher.group(1);
            String spripperPart = group.replaceAll("/GebedenBoek/msgGebedDataImage","").
                    replaceAll(".png","").
                    replaceAll(".jpg","").
                    replaceAll(".gif","").
                    replaceAll("<br>","<br />").
                    replaceAll("<hr>","<hr />");

            Long imageId = Long.valueOf(spripperPart);
            ImageDataTrf imageDataTrf = imageService.getMessageImageById(imageId);

            String encodeImg = "";
            if (imageDataTrf != null) {
                byte[] media = imageDataTrf.getFilebytes();
                encodeImg = Base64.encodeBytes(media);
            }
            String type = imageDataTrf.getFiletype();
            messageBody= messageBody.replaceAll(group,"data:" + type + ";base64,"+encodeImg);
        }
        return messageBody;
    }

    @Transactional
    public void saveNewMessage(MessageData newMessage) {
        MessagesTbl messagesTbl = new MessagesTbl();
        messagesTbl.setCreatedate(new Timestamp(new Date().getTime()));
        messagesTbl.setTitle(newMessage.getTitle());
        messagesTbl.setMessagebody(newMessage.getMessagebody());
        messagesRepo.save(messagesTbl);
    }

    @Transactional
    public void saveEditMessage(MessageData editMessage) {
        Optional<MessagesTbl>  optionTableMsg = messagesRepo.findById(editMessage.getId());
        if (optionTableMsg.isPresent()) {
            MessagesTbl editMessageTbl = optionTableMsg.get();
            editMessageTbl.setTitle(editMessage.getTitle());
            editMessageTbl.setMessagebody(editMessage.getMessagebody());

        }
    }

    public String construnctMessageForPerson(BezoekerData person, MessageData messageData){
        String gender = person.getGeslacht().equals("V") ? " mevrouw " : " meneer ";

        String fullMessage = "";

        fullMessage = "<h2>Geachte"+gender+  person.getFullName() + "</h2>";
        fullMessage += "<pre>"+messageData.getKopTekst().replace("\n","<br />") +"</pre><br />";
        fullMessage += messageData.getMessagebody();
        fullMessage += "&nbsp;<br />";
        fullMessage += "&nbsp;<br />";
        String slotTekst = messageData.getSlotTekst();
        slotTekst = "<p style=\"font-family:Arial;font-size:10pt;\">"+slotTekst.replace("\n","<br />");
        fullMessage +=  slotTekst + "</p>";
        fullMessage += "<br />";
        return fullMessage;
    }

    @Transactional
    public void prepareMessageToSend(MessageData messageData, Set<BezoekerData> recipientInfoList){
        for(BezoekerData bezoeker  :recipientInfoList) {
            try {
                String email  = bezoeker.getEmail();
                if (email == null || email.trim().length() == 0) {
                    continue;
                }

                String gender = bezoeker.getGeslacht().equals("V") ? " mevrouw " : " meneer ";
                String koptekst = "<h2>Geachte"+gender+  bezoeker.getFullName() + "</h2>" +
                        "<pre>"+messageData.getKopTekst().replace("\n","<br />") +"</pre><br />";
                String slotTekst =  "<p style=\"font-family:Arial;font-size:10pt;\">"+
                        messageData.getSlotTekst().replace("\n","<br />")+ "</p>";
                MessagesToSend messagesToSend = new MessagesToSend();
                messagesToSend.setMessageid(messageData.getId());
                messagesToSend.setReceiverid(bezoeker.getId());
                messagesToSend.setEmail(bezoeker.getEmail().trim());
                messagesToSend.setFullname(bezoeker.getFullName());
                messagesToSend.setCompleted(false);
                messagesToSend.setDatetosend(new Timestamp(new Date().getTime()));
                messagesToSend.setTitle(messageData.getTitle());
                messagesToSend.setKoptekst(koptekst);
                messagesToSend.setSlottekst(slotTekst);
                messagesToSendRepo.saveAndFlush(messagesToSend);
            } catch (Exception e) {
                log.warn(e.getMessage(), e);
            }
        }
    }

    @Transactional
    public void executeMessageSend(Long messageId, int port){
        Timestamp current = new Timestamp(new Date().getTime());
        String baseUrl = String.format("http://localhost:%d", port);

        Optional<AppsettingsTbl> optionSetting = appsettingsRepo.findById(INIT_MAIL);
        AppSetting appSetting = null;
        if (optionSetting.isPresent() ) {
            appSetting = TransferUtils.fromAppsettingsTbl(optionSetting.get());

        }
        if (appSetting == null) {
            return;
        }

        Optional<MessagesTbl> result = messagesRepo.findById(messageId);

        String  messageBody = result.isPresent() ? result.get().getMessagebody() : null;
        if(messageBody == null) {
            return;
        }

        Properties properties = fetchMailProperties(appSetting);
        Optional<List<MessagesToSend>> resultOptionList =   messagesToSendRepo.getMessageToSendList(messageId, current);
        if (resultOptionList.isPresent()) {
            List<MessagesToSend> resultList = resultOptionList.get();
            for(MessagesToSend messagesToSend : resultList) {
                try {
                    sendMailToPerson(baseUrl, messageBody, messagesToSend , properties, appSetting);
                    messagesToSendRepo.delete(messagesToSend);
                } catch (Exception e) {
                    log.warn(e.getMessage());

                }
            }
        }
    }



    private void sendMailToPerson(String baseURL, String messageBody, MessagesToSend messagesToSend,
                                  Properties properties, AppSetting appSetting) throws UnsupportedEncodingException, MessagingException {
        String fullMessage = constructFullMessage(messageBody, messagesToSend);

        InternetAddress inetReceiver = new InternetAddress(messagesToSend.getEmail(), messagesToSend.getFullname());

        Session session = Session.getInstance(properties,
                new SMTPAuthenticator(appSetting.getUsername(), appSetting.getWachtwoord()));

        //construct the text body part
        HtmlMessageImageData htmlMessageImageData = new HtmlMessageImageData(fullMessage);
        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setContent(htmlMessageImageData.getFinalMessage(), "text/html; charset=utf-8");
        //construct the mime multi part
        MimeMultipart mimeMultipart = new MimeMultipart();
        mimeMultipart.addBodyPart(bodyPart);

        Map<String, String > mapSrcToUid = htmlMessageImageData.getMapSrcToUid();
        for(String contentId : mapSrcToUid.keySet()) {
            String imgFileName =  mapSrcToUid.get(contentId);
            String imageFilePath = baseURL + imgFileName;
            try {
                MimeBodyPart imagePart = new MimeBodyPart();
                imagePart.setHeader("Content-ID", "<" + contentId + ">");

                imagePart.setDataHandler(new DataHandler(new URL(imageFilePath)));
                imagePart.setFileName(imgFileName);

                mimeMultipart.addBodyPart(imagePart);
            } catch (IOException e) {
                log.warn("Attachement Error: " + e.getMessage());
            }
        }
        //create the sender/recipient addresses
        InternetAddress iaSender = new InternetAddress(appSetting.getSender(), appSetting.getSendername());

        //construct the mime message
        MimeMessage mimeMessage = new MimeMessage(session);
        mimeMessage.setFrom(iaSender);
        mimeMessage.setSender(iaSender);
        mimeMessage.setSubject(messagesToSend.getTitle());
        mimeMessage.addRecipient(MimeMessage.RecipientType.TO, inetReceiver);
        mimeMessage.setContent(mimeMultipart);
        //send off the email
        Transport.send(mimeMessage);
    }



/*    private void sendMailToPerson(String baseURL,BezoekerData bezoeker, MessageData messageData,
                                  Properties properties,  AppSetting appSetting) throws UnsupportedEncodingException, MessagingException {
        String message = construnctMessageForPerson(bezoeker, messageData);
        InternetAddress inetReceiver = new InternetAddress(bezoeker.getEmail(), bezoeker.getFullName());

        Session session = Session.getInstance(properties,
                new SMTPAuthenticator(appSetting.getUsername(), appSetting.getWachtwoord()));
        //construct the text body part
        HtmlMessageImageData htmlMessageImageData = new HtmlMessageImageData(message);
        MimeBodyPart bodyPart = new MimeBodyPart();
        bodyPart.setContent(htmlMessageImageData.getFinalMessage(), "text/html; charset=utf-8");
        //construct the mime multi part
        MimeMultipart mimeMultipart = new MimeMultipart();
        mimeMultipart.addBodyPart(bodyPart);

        Map<String, String > mapSrcToUid = htmlMessageImageData.getMapSrcToUid();
        for(String contentId : mapSrcToUid.keySet()) {
            String imgFileName =  mapSrcToUid.get(contentId);
            String imageFilePath = baseURL + imgFileName;
            try {
                MimeBodyPart imagePart = new MimeBodyPart();
                imagePart.setHeader("Content-ID", "<" + contentId + ">");

                imagePart.setDataHandler(new DataHandler(new URL(imageFilePath)));
                imagePart.setFileName(imgFileName);

                mimeMultipart.addBodyPart(imagePart);
            } catch (IOException e) {
                log.warn("Attachement Error: " + e.getMessage());
            }
        }
        //create the sender/recipient addresses
        InternetAddress iaSender = new InternetAddress(appSetting.getSender(), appSetting.getSendername());

        //construct the mime message
        MimeMessage mimeMessage = new MimeMessage(session);
        mimeMessage.setFrom(iaSender);
        mimeMessage.setSender(iaSender);
        mimeMessage.setSubject(messageData.getTitle());
        mimeMessage.addRecipient(MimeMessage.RecipientType.TO, inetReceiver);
        mimeMessage.setContent(mimeMultipart);
        //send off the email
        Transport.send(mimeMessage);
    }*/


    private String readResource(final String fileName, Charset charset) throws IOException {
        return Resources.toString(Resources.getResource(fileName), charset);
    }



    private Properties fetchMailProperties(AppSetting appSetting ){
        Properties properties = new Properties();
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", appSetting.getHostname());
        properties.put("mail.smtp.port", appSetting.getPort());
        properties.put("mail.smtp.user", appSetting.getUsername());
        properties.put("mail.smtp.password", appSetting.getWachtwoord());
        return properties;
    }

    private String constructFullMessage(String messageBody, MessagesToSend messagesToSend){
        String fullMessage = messagesToSend.getKoptekst();
        fullMessage += messageBody;
        fullMessage += "&nbsp;<br />";
        fullMessage += "&nbsp;<br />";
        fullMessage +=  messagesToSend.getSlottekst();
        fullMessage += "<br />" ;
        return fullMessage;
    }


    public static void main(String[]args) throws IOException {

        final String username = "appArwLutchman@outlook.com";
        final String password = "xxxxxx";

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.live.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("appArwLutchman@outlook.com"));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse("arwlutchman@gmail.com"));
            message.setSubject("Test");
            message.setText("HI");

            Transport.send(message);

            System.out.println("Done");

        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
