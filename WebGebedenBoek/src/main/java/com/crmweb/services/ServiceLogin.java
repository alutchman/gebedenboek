package com.crmweb.services;

import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.repo.AppsettingsRepo;
import com.domain.ukgr.repo.WebUserRepo;
import com.domain.ukgr.tables.AppsettingsTbl;
import com.domain.ukgr.tables.Webuser;
import com.shared.transfers.AppSetting;
import com.shared.transfers.SearchUser;
import com.shared.transfers.UserLevel;
import com.shared.transfers.WebUserData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.crmweb.services.MessagesService.INIT_MAIL;

@Service
public class ServiceLogin {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceLogin.class);
    private static final String MAIN_ADMIN = "admin";
    private static final String INIT_PW = "welkom01";



    @Autowired
    private WebUserRepo webUserRepo;

    @Autowired
    private AppsettingsRepo appsettingsRepo;

    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        ensureAdmin();
    }

    public boolean login(String username, String password, HttpServletRequest request,HttpSession session ) {
        Optional<Webuser> entityOptional = webUserRepo.finduser(username,createHashData(password));
        Webuser entity = entityOptional.isPresent() ? entityOptional.get() : null;
        if (entity != null) {
            WebUserData webUserData = TransferUtils.fromWebuser(entity);
            session.invalidate();
            HttpSession newSession = request.getSession();
            newSession.setAttribute(WebUserData.class.getSimpleName(),webUserData);

            return true;
        } else {
            return false;
        }
    }


    @Transactional
    public boolean wachtwoordNaInloggen(String oldPassword, String newPassword, WebUserData alterUser){
        Webuser entity;
        if ( (oldPassword == null || oldPassword.trim().length() == 0 ) &&
             (newPassword == null || newPassword.trim().length() == 0 )  ){
            Optional<Webuser> entityOptional = webUserRepo.finduser(alterUser.getUserid());
            entity = entityOptional.isPresent() ? entityOptional.get() : null;

            if (entity != null) {
                entity.setVoornaam(alterUser.getVoornaam());
                entity.setAchternaam(alterUser.getAchternaam());
            }
        } else {
            Optional<Webuser> entityOptional = webUserRepo.finduser(alterUser.getUserid(),
                    createHashData(oldPassword.trim()));
            entity = entityOptional.isPresent() ? entityOptional.get() : null;

            if (entity != null) {
                entity.setVoornaam(alterUser.getVoornaam());
                entity.setAchternaam(alterUser.getAchternaam());
                entity.setWachtwoord(createHashData(newPassword.trim()));
            }
        }
        return entity != null;
    }

    @Transactional
    public void ensureAdmin()  {
        LOGGER.info("Admin registation.....");
        Optional<Webuser> entityOptional = webUserRepo.finduser(MAIN_ADMIN);

        if (!entityOptional.isPresent()) {
            Webuser webuser = new Webuser();
            webuser.setUserid(MAIN_ADMIN);
            webuser.setGroupnaam(UserLevel.BEHEER.name());
            webuser.setAchternaam("Applicatie");
            webuser.setVoornaam("Beheer");
            webuser.setWachtwoord(createHashData(INIT_PW));
            webUserRepo.save(webuser);
        }
    }

    private String createHashData(String input)  {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
            md.update(input.getBytes());

            byte byteData[] = md.digest();

            String digested = DatatypeConverter
                    .printHexBinary(byteData).toUpperCase();
            return digested;
        } catch (NoSuchAlgorithmException e) {

            return null;
        }
    }


    public List<WebUserData> haalGebruikersOp(SearchUser searchUser, String excudeUserId) {
        if (searchUser.getVoornaam().trim().length() == 0) {
            searchUser.setVoornaam("%");
        }
        Optional<List<Webuser>> webuserList = webUserRepo.searchUser(searchUser.getVoornaam(),searchUser.getAchternaam(), excudeUserId);

        List<Webuser> results = webuserList.isPresent() ? webuserList.get() : new ArrayList<Webuser>();
        List<WebUserData> exportList = new ArrayList<>();
        for (Webuser entity : results) {
            exportList.add(TransferUtils.fromWebuser(entity));
        }

        return exportList;
    }

    @Transactional
    public void updateUser(WebUserData currentUser) {
        Optional<Webuser> entityOptional = webUserRepo.finduser(currentUser.getUserid());
        Webuser entity = entityOptional.isPresent() ? entityOptional.get() : null;
        if (entity != null) {
            entity.setAchternaam(currentUser.getAchternaam());
            entity.setVoornaam(currentUser.getVoornaam());
            entity.setGroupnaam(currentUser.getCurrentlevel().name());
        }
    }

    public boolean isUserIdFree(WebUserData newUser) {
        Optional<Webuser> entityOptional = webUserRepo.finduser(newUser.getUserid());
        return !entityOptional.isPresent();
    }

    @Transactional
    public void addNewUser(WebUserData newUser) {
        Webuser webuser = new Webuser();
        webuser.setUserid(newUser.getUserid());
        webuser.setGroupnaam(newUser.getCurrentlevel().name());
        webuser.setAchternaam(newUser.getAchternaam());
        webuser.setVoornaam(newUser.getVoornaam());
        webuser.setWachtwoord(createHashData(INIT_PW));
        webUserRepo.save(webuser);
    }

    public AppSetting getMailSettings(){
        Optional<AppsettingsTbl> optionSetting = appsettingsRepo.findById(INIT_MAIL);
        if (optionSetting.isPresent()) {
            return TransferUtils.fromAppsettingsTbl(optionSetting.get());
        }

        AppSetting appSetting = new AppSetting();
        appSetting.setIdent(INIT_MAIL);
        return appSetting;
    }

    @Transactional
    public void updateMailSettings(AppSetting appSetting) {
        Optional<AppsettingsTbl> optionSetting = appsettingsRepo.findById(INIT_MAIL);
        if (optionSetting.isPresent()) {
            AppsettingsTbl mailSetting = optionSetting.get();
            mailSetting.setHostname(appSetting.getHostname());
            mailSetting.setPort(appSetting.getPort());
            mailSetting.setSender(appSetting.getSender());
            mailSetting.setSendername(appSetting.getSendername());
            mailSetting.setUsername(appSetting.getUsername());
            mailSetting.setWachtwoord(appSetting.getWachtwoord());
        }
        appsettingsRepo.flush();
    }
}
