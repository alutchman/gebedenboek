package com.crmweb.services;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Getter
@PropertySource("classpath:ukgrapp.properties")
@Component
public class UkgrProprtiesApplication implements Serializable {
    private static final long serialVersionUID = 2159331661582451156L;

    @Value("${ukgr.property.location}")
    private String location;

    @Value("${ukgr.property.director}")
    private String director;

    @Value("${ukgr.property.watermark}")
    private String watermark;


    @Value("${ukgr.property.title}")
    private String pageTitle;

    @Value("${ukgr.property.footer}")
    private String pageFooter;


    @Value("${ukgr.property.pdfTitle}")
    private String pdfTitle;



    @Value("${ukgr.property.headerTitle}")
    private String headerTitle;


    @Value("${ukgr.property.pdfKeywords}")
    private String pdfKeywords;


    @Value("${ukgr.property.pdfAuthor}")
    private String pdfAuthor;


}
