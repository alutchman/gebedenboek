package com.crmweb.services;

import com.crmweb.utils.TransferUtilBezoeker;
import com.domain.ukgr.repo.BezoekerRepo;
import com.domain.ukgr.repo.FilesDataRepo;
import com.domain.ukgr.tables.BezoekerTbl;
import com.domain.ukgr.tables.FilesdataTbl;
import com.shared.transfers.BezoekerData;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.model.UploadedFileWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class BezoekerService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BezoekerService.class);

    @Autowired
    private BezoekerRepo bezoekerRepo;

    @Autowired
    private FilesDataRepo filesDataRepo;

    @Autowired
    private ImageService imageService;


    @Transactional
    public List<BezoekerData> findBezoeker(String email, String voornaam, String achternaam){
        if (email != null && email.trim().length() > 0 && voornaam != null && voornaam.trim().length() >0) {
            Optional<List<BezoekerTbl>> bezoekerTblOptional = bezoekerRepo.findByEmailAndFirstName(email.trim(), voornaam.trim());
            return fillResultListBezoeker(bezoekerTblOptional);
        } else if (email != null && email.trim().length() > 0 && achternaam != null && achternaam.trim().length() >0)   {
            Optional<List<BezoekerTbl>>  bezoekerTblOptional = bezoekerRepo.findByLastNameAndEmail(email.trim(), achternaam.trim());
            return fillResultListBezoeker(bezoekerTblOptional);
        } else if (achternaam != null && achternaam.trim().length() > 0 && voornaam != null && voornaam.trim().length() >0)   {
            Optional<List<BezoekerTbl>>  bezoekerTblOptional = bezoekerRepo.findByNames(achternaam.trim()+"%", voornaam.trim()+"%");
            return fillResultListBezoeker(bezoekerTblOptional);
        } else if (achternaam != null && achternaam.trim().length() > 0 )   {
            Optional<List<BezoekerTbl>>  bezoekerTblOptional = bezoekerRepo.findByLastNameLike(achternaam.trim()+"%");
            return fillResultListBezoeker(bezoekerTblOptional);
        } else if (email != null && email.trim().length() > 0 )   {
            Optional<List<BezoekerTbl>>  bezoekerTblOptional = bezoekerRepo.findByEmail(email.trim());
            return fillResultListBezoeker(bezoekerTblOptional);
        }
        return new ArrayList<>();
    }

    private List<BezoekerData> fillResultListBezoeker(Optional<List<BezoekerTbl>> bezoekerTblOptional){
        List<BezoekerData> resultList = new ArrayList<>();
        if (bezoekerTblOptional.isPresent()) {
            List<BezoekerTbl> dbResultList = bezoekerTblOptional.get();
            dbResultList.forEach(entity -> resultList.add(TransferUtilBezoeker.fromBezoekerTbl(entity)));
        }
        return resultList;
    }

    @Transactional
    public void addBezoeker(BezoekerData newBezoeker, UploadedFileWrapper newBezoekersImage) {
        if (newBezoekersImage != null && newBezoekersImage.getFileName().length() > 0) {
            FilesdataTbl resultFile =  imageService.addNewFile(newBezoekersImage);
            newBezoeker.setImageid(resultFile.getId());
        }
        BezoekerTbl bezoekerTbl = TransferUtilBezoeker.fromBezoekerData(newBezoeker);
        bezoekerRepo.save(bezoekerTbl);
    }




    @Transactional
    public void deleteBezoeker(BezoekerData currentBezoeker) {
        Optional<BezoekerTbl> bezoekerTblOptional =  bezoekerRepo.findById(currentBezoeker.getId());
        if (bezoekerTblOptional.isPresent()) {
            BezoekerTbl bezoekerTbl = bezoekerTblOptional.get();
            bezoekerRepo.delete(bezoekerTbl);
        }
        if (currentBezoeker.getImageid() != null  && currentBezoeker.getImageid() >0) {
            Optional<FilesdataTbl> imageDataOpt =  filesDataRepo.findById(currentBezoeker.getImageid());
            if (imageDataOpt.isPresent()) {
                filesDataRepo.delete(imageDataOpt.get());
            }
        }
    }

    @Transactional
    public void updateBezoeker(BezoekerData currentBezoeker, String newEmail, UploadedFileWrapper newBezoekersImage) {
        FilesdataTbl orgImage = null;
        if (currentBezoeker.getImageid() != null  && currentBezoeker.getImageid() >0) {
            Optional<FilesdataTbl> imageDataOpt =  filesDataRepo.findById(currentBezoeker.getImageid());
            if (imageDataOpt.isPresent()) {
                orgImage = imageDataOpt.get();
            }
        }
        if (newBezoekersImage != null && newBezoekersImage.getFileName() != null
                && newBezoekersImage.getFileName().trim().length() > 0) {
            if (orgImage != null) {
                orgImage.setCreatedate(new Timestamp(new Date().getTime()));
                orgImage.setExtension(FilenameUtils.getExtension(newBezoekersImage.getFileName()));
                orgImage.setFilebytes(newBezoekersImage.getContents());
                orgImage.setFiletype(newBezoekersImage.getContentType());
                orgImage.setTitle(newBezoekersImage.getFileName());
            } else {
                FilesdataTbl resultFile =  imageService.addNewFile(newBezoekersImage);
                currentBezoeker.setImageid(resultFile.getId());
            }
        }
        Optional<BezoekerTbl> bezoekerTblOptional =  bezoekerRepo.findById(currentBezoeker.getId());
        if (bezoekerTblOptional.isPresent()) {
            BezoekerTbl data2Update = bezoekerTblOptional.get();
            if (newEmail != null && newEmail.trim().length() > 0) {
                currentBezoeker.setEmail(newEmail.trim());
            }
            TransferUtilBezoeker.updateWithBezoekerData(data2Update, currentBezoeker);
        }
    }

    public List<BezoekerData> findBezoeker(Long selectedSendLocation) {
        List<BezoekerData> resultList = new ArrayList<>();

        BezoekerTbl visitor = new BezoekerTbl();
        visitor.setLocatieID(selectedSendLocation);
        Example<BezoekerTbl> visitorExample = Example.of(visitor);

        List<BezoekerTbl> dbResultList = bezoekerRepo.findAll(visitorExample,Sort.by(Sort.Direction.ASC,
                "achternaam","voornaam","postcode"));
        dbResultList.forEach(entity -> resultList.add(TransferUtilBezoeker.fromBezoekerTbl(entity)));

        return resultList;
    }
}
