package com.crmweb.services;

import com.crmweb.utils.TransferUtils;
import com.domain.ukgr.repo.LocatieRepo;
import com.domain.ukgr.tables.Locaties;
import com.shared.transfers.LocatieData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LocatiesService {
    @Autowired
    private LocatieRepo locatieRepo;

    public List<LocatieData> findAllLocations(){
        List<LocatieData> locatieList = new ArrayList<>();
        Optional<List<Locaties>> resultOptional = locatieRepo.findLocationsSorted();
        if (resultOptional.isPresent()) {
            List<Locaties> dbResultList = resultOptional.get();
            dbResultList.forEach(entity -> locatieList.add(TransferUtils.fromLocaties(entity)));
        }
        return locatieList;
    }

    @Transactional
    public void addNewLocatie(LocatieData newLocatie) {
        Locaties newloc =  TransferUtils.toLocaties(newLocatie);
        locatieRepo.save(newloc);
    }

    @Transactional
    public void updateLocatie(LocatieData editLocatie) {
        Optional<Locaties> locOpt =  locatieRepo.findById(editLocatie.getId());
        if (locOpt.isPresent()) {
            Locaties locUpdate = locOpt.get();
            locUpdate.setNummer(editLocatie.getNummer());
            locUpdate.setToevoeging(editLocatie.getToevoeging());
            locUpdate.setPostcode(editLocatie.getPostcode());
            locUpdate.setStraatnaam(editLocatie.getStraatnaam());
            locUpdate.setWoonplaats(editLocatie.getWoonplaats());
            locUpdate.setUkgrcode(editLocatie.getUkgrcode());
            locUpdate.setTelefoon(editLocatie.getTelefoon());
        }
    }


    @Transactional
    public LocatieData findLocatie(Long currentLocatie) {
        Optional<Locaties> locOpt =  locatieRepo.findById(currentLocatie);
        if (locOpt.isPresent()) {
            return TransferUtils.fromLocaties(locOpt.get());
        }
        return null;
    }

    @Transactional
    public LocatieData findUkgrCodeLocatie(Integer ukgrcode,Long  locatieId){
        Optional<Locaties> locOpt =  locatieRepo.findLocatiesByUkgrCode(ukgrcode, locatieId);
        if (locOpt.isPresent()) {
            return TransferUtils.fromLocaties(locOpt.get());
        }
        return null;
    }

}
