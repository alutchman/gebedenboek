package com.crmweb.email;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Getter
public class HtmlMessageImageData {
    private static final Pattern IMG_PATTERN = Pattern.compile(
            "<img(\\s+.*?)(?:src=\"(.*?)\")(.*?)>");

    private Map<String, String > mapSrcToUid = new HashMap<>();

    private String initialMessage;

    private String finalMessage;


    public HtmlMessageImageData(String initialMessage){
        this.initialMessage = initialMessage;
        this.finalMessage = rewriteImgTag();
    }

    private String rewriteImgTag() {
        StringBuilder sb = new StringBuilder();
        Matcher m = IMG_PATTERN.matcher(initialMessage);
        int start = 0;
        while (m.find()) {
            String imageSrc = m.group(2);
            String uid = UUID.nameUUIDFromBytes(imageSrc.getBytes()).toString();
            String replaceid = "cid:"+(uid);
            /*
             This map will be used to fetch the image and
             add it after the mime boundary with the id uid.

             The map assures that if a same image is used, only one attachement is sent.
             */
            mapSrcToUid.put(uid, imageSrc);

            int end = m.start();
            sb.append(initialMessage.substring(start, end));

            sb.append("<img");

            for (int i = 1; i < m.groupCount() + 1; i++) {
                if (i == 2) { // image src
                    sb.append(" src=\""+ replaceid +"\"");
                    continue;
                }

                sb.append(m.group(i));
            }
            sb.append("/>");


            start = m.end();
        }
        sb.append(initialMessage.substring(start, initialMessage.length()));
        return sb.toString();
    }

    /*
    public static void main(String[] args){
        String html = "";
        HtmlMessageImageData htmlMessageImageData = new HtmlMessageImageData(html);
        String baseURL = String.format("http://localhost:%d", 8080);

        Map<String, String > mapSrcToUid = htmlMessageImageData.getMapSrcToUid();
        for(String contentId : mapSrcToUid.keySet()) {
            String imgFileName = mapSrcToUid.get(contentId);
            String imageFilePath = baseURL +  imgFileName;
            System.out.printf("image srv = %s", imageFilePath);
        }
    }
    */

}
