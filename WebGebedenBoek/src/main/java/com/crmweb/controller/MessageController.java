package com.crmweb.controller;

import com.crmweb.services.ImageService;
import com.crmweb.services.MessagesService;
import com.shared.transfers.ImageDataTrf;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Controller
public class MessageController {
    private static final String START_HTML = "<!doctype html>\n" +
            "<html lang=\"en\">\n" +
            "<head>\n" +
            "  <meta charset=\"utf-8\">\n" +
            "  <title>GebedenBoek boodschap</title>\n" +
            "  <meta name=\"description\" content=\"ukgr.nl\">\n" +
            "  <meta name=\"author\" content=\"ukgradmin\">\n" +
            "</head>\n" +
            "<body>\n";

    private static final String END_HTML = "\n</body>\n" +
            "</html>";

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ImageService imageService;

    @Autowired
    private MessagesService messagesService;

    @RequestMapping(value="gebed.html")
    public ModelAndView settings(){
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        return new ModelAndView("message/index",datamap);
    }

    @RequestMapping(value = ImageDataTrf.BASE_IMG_NAME + "{id}.{ext}", method = RequestMethod.GET, produces = MediaType.ALL_VALUE)
    public ResponseEntity<byte[]> imgFromMessage(@PathVariable("id") Long id) throws IOException {
        HttpHeaders headers = new HttpHeaders();

        ImageDataTrf imageDataTrf = imageService.getMessageImageById(id);

        if (imageDataTrf == null) {
            byte[] error = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("bird_heart.png"));
            return new ResponseEntity<>(error, headers, HttpStatus.OK);
        }

        byte[] media = imageDataTrf.getFilebytes();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        headers.setContentLength(media.length);
        headers.setContentType(MediaType.valueOf(imageDataTrf.getFiletype()));
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
        return responseEntity;
    }


    @RequestMapping(value = ImageDataTrf.BASE_CONTENT_NAME + "{id}.html", method = RequestMethod.GET, produces = MediaType.TEXT_HTML_VALUE)
    public ResponseEntity<String> contentFromMessage(@PathVariable("id") Long id, HttpServletRequest request) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "text/html; charset=utf-8");

        String messageData = messagesService.findWithEmbeddedImgMessageById(id);
        StringBuffer message2Send = new StringBuffer(START_HTML);
        message2Send.append(messageData);
        message2Send.append(END_HTML);
        return  new ResponseEntity<>(message2Send.toString(), headers, HttpStatus.OK);
    }
}
