package com.crmweb.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ErrorController {
	// Total control - setup a model and return the view name yourself. Or
	// consider subclassing ExceptionHandlerExceptionResolver (see below).
	@ExceptionHandler({RuntimeException.class,NullPointerException.class})
	public ModelAndView handleError(HttpServletRequest req, RuntimeException ex) {
		return buildErrorPage(req, ex);
	}


	@RequestMapping(value = {
			"{path:(?!webjars|static).*$}",
			"{path:(?!webjars|static).*$}/**",
			"{path:(?!webjars|static).*$}/**.html"
	},
			headers={"Accept=application/xml", "Accept=text/html"},
			method = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, RequestMethod.TRACE}
	)
	public ModelAndView pageNotFound(HttpServletRequest req, Exception ex){
		return buildErrorPage(req, ex);
	}

	private ModelAndView buildErrorPage(HttpServletRequest req, Exception ex){
		ModelAndView mav = new ModelAndView();
		mav.addObject("exception", ex);
		mav.addObject("url", req.getRequestURL());
		mav.setViewName("auth/error");
		return mav;
	}
}
