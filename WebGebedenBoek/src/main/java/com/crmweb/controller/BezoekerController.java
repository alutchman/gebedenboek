package com.crmweb.controller;

import com.crmweb.services.ImageService;
import com.shared.transfers.FileDataTrf;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BezoekerController {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    private ImageService imageService;


    @RequestMapping(value="bezoekers.html")
    public ModelAndView settings(){
        Map<String, Object> datamap = new HashMap<String, Object>();
        datamap.put("baseUrl", applicationContext.getApplicationName());
        return new ModelAndView("bezoekers/index",datamap);
    }


    @RequestMapping(value = "fileData{id}.{ext}", method = RequestMethod.GET, produces = MediaType.ALL_VALUE)
    public ResponseEntity<byte[]> tmpEmailJpeg(@PathVariable("id") Long id) throws IOException {
        HttpHeaders headers = new HttpHeaders();

        FileDataTrf fileDataTrf = imageService.getImageById(id);

        if (fileDataTrf == null) {
            byte[] error = IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("bird_heart.png"));
            return new ResponseEntity<>(error, headers, HttpStatus.NOT_FOUND);
        }

        byte[] media = fileDataTrf.getFilebytes();
        headers.setCacheControl(CacheControl.noCache().getHeaderValue());
        headers.setContentLength(media.length);
        headers.setContentType(MediaType.valueOf(fileDataTrf.getFiletype()));
        ResponseEntity<byte[]> responseEntity = new ResponseEntity<>(media, headers, HttpStatus.OK);
        return responseEntity;
    }
}
