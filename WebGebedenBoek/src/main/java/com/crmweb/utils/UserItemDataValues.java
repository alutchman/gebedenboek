package com.crmweb.utils;

import com.shared.transfers.BezoekerData;
import lombok.Data;

@Data
public class UserItemDataValues {
    private BezoekerData bezoeker;
    private String prayerDataHtml;
    private byte[] prayerDataPdf;


}
