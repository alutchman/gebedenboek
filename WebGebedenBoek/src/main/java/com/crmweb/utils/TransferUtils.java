package com.crmweb.utils;

import com.domain.ukgr.tables.*;
import com.shared.transfers.*;
import org.apache.commons.lang3.StringUtils;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public final class TransferUtils {
    private TransferUtils(){
        //===SINGLETON
    }

    public static MessageData fromMessageTbl(MessagesTbl messagesTbl) {
        MessageData messageData = new MessageData();
        messageData.setId(messagesTbl.getId());
        messageData.setCreatedate(messagesTbl.getCreatedate());
        messageData.setTitle(messagesTbl.getTitle());
        messageData.setMessagebody(messagesTbl.getMessagebody());

        String body = messagesTbl.getMessagebody();
        messageData.setMessagebodyTruncate(StringUtils.abbreviate(body, 60));
        return messageData;
    }


    public static List<MessageData> fromMessageTbl(List<MessagesTbl> messagesTblList) {
        List<MessageData> resultList = new ArrayList<>();
        for (MessagesTbl messagesTbl : messagesTblList)  {
            MessageData messageData = fromMessageTbl(messagesTbl);
            resultList.add(messageData);
        }
        return resultList;
    }


    public static void updateWithMessageData(MessagesTbl messagesTbl, MessageData messageData ) {
        messagesTbl.setCreatedate(new Timestamp(messageData.getCreatedate().getTime()));
        messagesTbl.setTitle(messageData.getTitle());
        messagesTbl.setMessagebody(messageData.getMessagebody());
    }

    public static WebUserData fromWebuser(Webuser webuser){
        WebUserData webUserData = new WebUserData();
        webUserData.setUserid(webuser.getUserid());
        webUserData.setAchternaam(webuser.getAchternaam());
        webUserData.setVoornaam(webuser.getVoornaam());

        webUserData.setCurrentlevel(UserLevel.GEEN);

        String groupType = webuser.getGroupnaam();
        for (UserLevel userLevel : UserLevel.values()) {
            if (userLevel.name().equals(groupType)) {
                webUserData.setCurrentlevel(userLevel);
            }
        }

        webUserData.setValidUser(true);
        return webUserData;
    }


    public static ImageDataTrf fromMessageImageTbl(MessageImageTbl messageImageTbl){
        ImageDataTrf imageDataTrf = new ImageDataTrf();
        imageDataTrf.setId(messageImageTbl.getId());
        imageDataTrf.setCreatedate(messageImageTbl.getCreatedate());
        imageDataTrf.setExtension(messageImageTbl.getExtension());
        imageDataTrf.setFilebytes(messageImageTbl.getFilebytes());
        imageDataTrf.setTitle(messageImageTbl.getTitle());
        imageDataTrf.setFiletype(messageImageTbl.getFiletype());
        return imageDataTrf;

    }

    public static AppSetting fromAppsettingsTbl(AppsettingsTbl tblData){
        AppSetting appSetting = new AppSetting();
        appSetting.setIdent(tblData.getIdent());
        appSetting.setHostname(tblData.getHostname());
        appSetting.setPort(tblData.getPort());
        appSetting.setSender(tblData.getSender());
        appSetting.setUsername(tblData.getUsername());
        appSetting.setWachtwoord(tblData.getWachtwoord());
        appSetting.setSendername(tblData.getSendername());
        return appSetting;
    }

    public static Locaties toLocaties(LocatieData newLocatie) {
        Locaties locatieData = new Locaties();
        locatieData.setId(newLocatie.getId());
        locatieData.setNummer(newLocatie.getNummer());
        locatieData.setToevoeging(newLocatie.getToevoeging());
        locatieData.setPostcode(newLocatie.getPostcode());
        locatieData.setStraatnaam(newLocatie.getStraatnaam());
        locatieData.setTelefoon(newLocatie.getTelefoon());
        locatieData.setWoonplaats(newLocatie.getWoonplaats());
        locatieData.setUkgrcode(newLocatie.getUkgrcode());
        return locatieData;
    }


    public static LocatieData fromLocaties(Locaties locaties){
        LocatieData locatieData = new LocatieData();
        locatieData.setId(locaties.getId());
        locatieData.setNummer(locaties.getNummer());
        locatieData.setToevoeging(locaties.getToevoeging());
        locatieData.setPostcode(locaties.getPostcode());
        locatieData.setStraatnaam(locaties.getStraatnaam());
        locatieData.setTelefoon(locaties.getTelefoon());
        locatieData.setWoonplaats(locaties.getWoonplaats());
        locatieData.setUkgrcode(locaties.getUkgrcode());
        return locatieData;
    }


}
