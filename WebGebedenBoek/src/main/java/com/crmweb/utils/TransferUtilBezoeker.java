package com.crmweb.utils;

import com.domain.ukgr.tables.BezoekerTbl;
import com.domain.ukgr.tables.FilesdataTbl;
import com.shared.transfers.BezoekerData;
import com.shared.transfers.FileDataTrf;

import java.sql.Timestamp;

public final class TransferUtilBezoeker {
    private TransferUtilBezoeker(){
        //===SINGLETON
    }


    public static BezoekerTbl fromBezoekerData(BezoekerData bezoekerData){
        BezoekerTbl bezoekerTbl = new BezoekerTbl();
        updateWithBezoekerData(bezoekerTbl, bezoekerData);
        return bezoekerTbl;
    }

    public static void updateWithBezoekerData(BezoekerTbl bezoekerTbl, BezoekerData bezoekerData){
        bezoekerTbl.setEmail(bezoekerData.getEmail());
        bezoekerTbl.setImageid(bezoekerData.getImageid());
        bezoekerTbl.setAchternaam(bezoekerData.getAchternaam());
        bezoekerTbl.setGeboortedatum(new Timestamp(bezoekerData.getGeboortedatum().getTime()));
        bezoekerTbl.setGeslacht(bezoekerData.getGeslacht());
        bezoekerTbl.setHoecontactkerk(bezoekerData.getHoecontactkerk());
        bezoekerTbl.setNummer(bezoekerData.getNummer());
        bezoekerTbl.setPostcode(bezoekerData.getPostcode());
        bezoekerTbl.setLocatieID(bezoekerData.getLocatieId());
        bezoekerTbl.setStraatnaam(bezoekerData.getStraatnaam());
        bezoekerTbl.setTelefoonDag(bezoekerData.getTelefoonDag());
        bezoekerTbl.setTelefoonNacht(bezoekerData.getTelefoonNacht());
        bezoekerTbl.setVoornaam(bezoekerData.getVoornaam());
        bezoekerTbl.setWoonplaats(bezoekerData.getWoonplaats());
        bezoekerTbl.setToestemming(bezoekerData.getToestemming());
    }

    public static BezoekerData fromBezoekerTbl(BezoekerTbl bezoekerTbl){
        BezoekerData bezoekerData = new BezoekerData();
        bezoekerData.setId(bezoekerTbl.getId());

        bezoekerData.setEmail(bezoekerTbl.getEmail());
        bezoekerData.setImageid(bezoekerTbl.getImageid());
        bezoekerData.setAchternaam(bezoekerTbl.getAchternaam());
        bezoekerData.setGeboortedatum(bezoekerTbl.getGeboortedatum());
        bezoekerData.setGeslacht(bezoekerTbl.getGeslacht());
        bezoekerData.setHoecontactkerk(bezoekerTbl.getHoecontactkerk());
        bezoekerData.setNummer(bezoekerTbl.getNummer());
        bezoekerData.setPostcode(bezoekerTbl.getPostcode());
        bezoekerData.setLocatieId(bezoekerTbl.getLocatieID());
        bezoekerData.setStraatnaam(bezoekerTbl.getStraatnaam());
        bezoekerData.setTelefoonDag(bezoekerTbl.getTelefoonDag());
        bezoekerData.setTelefoonNacht(bezoekerTbl.getTelefoonNacht());
        bezoekerData.setVoornaam(bezoekerTbl.getVoornaam());
        bezoekerData.setWoonplaats(bezoekerTbl.getWoonplaats());
        bezoekerData.setToestemming(bezoekerTbl.getToestemming());
        return bezoekerData;
    }

    public static FileDataTrf fromFilesdataTbl(FilesdataTbl filesdataTbl){
        FileDataTrf fileDataTrf = new FileDataTrf();
        fileDataTrf.setId(filesdataTbl.getId());
        fileDataTrf.setCreatedate(filesdataTbl.getCreatedate());
        fileDataTrf.setExtension(filesdataTbl.getExtension());
        fileDataTrf.setFilebytes(filesdataTbl.getFilebytes());
        fileDataTrf.setTitle(filesdataTbl.getTitle());
        fileDataTrf.setFiletype(filesdataTbl.getFiletype());
        return fileDataTrf;

    }

}
