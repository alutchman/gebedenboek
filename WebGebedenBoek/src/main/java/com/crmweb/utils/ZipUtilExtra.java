package com.crmweb.utils;


import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * This utility compresses a list of files to standard ZIP format file.
 * It is able to compresses all sub files and sub directories, recursively.
 * @author Ha Minh Nam
 *
 */
public class ZipUtilExtra {
    /**
     * A constants for buffer size used to read/write data
     */
    private static final int BUFFER_SIZE = 4096;

    private final String sourcePath;
    private final File folder;
    private final List<File> listFiles;

    private int totalBytesHandled = 0;

    public ZipUtilExtra(String sourcePath) {
        this.sourcePath = sourcePath;
        folder = new File(sourcePath);

        if (folder.exists()) {
            File[] listOfFiles = folder.listFiles();
            listFiles = Arrays.asList(listOfFiles);
        } else {
            listFiles = new ArrayList<>();
        }

    }


    public void compressFiles(OutputStream os) throws FileNotFoundException, IOException {
        ZipOutputStream zos = new ZipOutputStream(os);
        for (File file : listFiles) {
            if (!file.exists()) {
                continue;
            }
            if (file.isDirectory()) {
                addFolderToZip(file, file.getName(), zos);
            } else {
                addFileToZip(file, zos);
            }
        }
        zos.flush();
        zos.close();
    }

    public int getTotalBytesHandled() {
        return totalBytesHandled;
    }

    /**
     * Adds a directory to the current zip output stream
     * @param folder the directory to be  added
     * @param parentFolder the path of parent directory
     * @param zos the current zip output stream
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void addFolderToZip(File folder, String parentFolder,
                                ZipOutputStream zos) throws FileNotFoundException, IOException {
        for (File file : folder.listFiles()) {
            if (file.isDirectory()) {
                addFolderToZip(file, parentFolder + "/" + file.getName(), zos);
                continue;
            }

            zos.putNextEntry(new ZipEntry(parentFolder + "/" + file.getName()));

            InputStream is = new FileInputStream(file);
            BufferedInputStream bis = new BufferedInputStream(is);

            long bytesRead = 0;
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read = 0;

            while ((read = bis.read(bytesIn)) != -1) {
                zos.write(bytesIn, 0, read);
                bytesRead += read;
            }

            zos.closeEntry();
            is.close();
        }
    }

    /**
     * Adds a file to the current zip output stream
     * @param file the file to be added
     * @param zos the current zip output stream
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void addFileToZip(File file, ZipOutputStream zos)
            throws FileNotFoundException, IOException {
        zos.putNextEntry(new ZipEntry(file.getName()));

        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(
                file));

        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read;

        while ((read = bis.read(bytesIn)) != -1) {
            zos.write(bytesIn, 0, read);
            this.totalBytesHandled += read;
        }

        zos.closeEntry();
    }
}
