package com.crmweb.utils;


import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;


/**
 * This utility compresses a list of files to standard ZIP format file.
 * It is able to compresses all sub files and sub directories, recursively.
 * @author Ha Minh Nam
 *
 */
public class ZipDataUsersUtils {
    /**
     * A constants for buffer size used to read/write data
     */
    private static final int BUFFER_SIZE = 4096;



    private int totalBytesHandled = 0;



    public void compressZipData(OutputStream os, List<UserItemDataValues> userdataList) throws IOException {
        ZipOutputStream zos = new ZipOutputStream(os);
        for (UserItemDataValues userItemDataValue : userdataList) {
            addVisitorDataItemToZip(userItemDataValue, zos);
        }
        zos.flush();
        zos.close();
    }

    public int getTotalBytesHandled() {
        return totalBytesHandled;
    }


    /**
     * Adds a file to the current zip output stream
     * @param fileUserData the file to be added
     * @param zos the current zip output stream
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void addVisitorDataItemToZip(UserItemDataValues fileUserData, ZipOutputStream zos)
            throws FileNotFoundException, IOException {
        String fileName = fileUserData.getBezoeker().getFileNameForPdf();
        zos.putNextEntry(new ZipEntry(fileName));

        InputStream fileData = new ByteArrayInputStream(fileUserData.getPrayerDataPdf());


        BufferedInputStream bis = new BufferedInputStream(fileData);

        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read;

        while ((read = bis.read(bytesIn)) != -1) {
            zos.write(bytesIn, 0, read);
            this.totalBytesHandled += read;
        }

        zos.closeEntry();
    }
}
