package com.crmweb.jsfbean;

import com.crmweb.utils.MenuDef;
import com.shared.transfers.WebUserData;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@ManagedBean
@RequestScoped
public class MenuBean {
    private String contextPath;
    private final List<MenuDef> menuList = new ArrayList<>();

    private String logoutUrl;

    private int currentYear;

    @PostConstruct
    public void init() {
        currentYear = Calendar.getInstance().get(Calendar.YEAR);
        contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
        logoutUrl = contextPath + "/logout.html";
        WebUserData webUserData = (WebUserData) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get(WebUserData.class.getSimpleName());
        if (webUserData == null) {
            return;
        }

        if (webUserData.getCurrentlevel().ordinal() < 3) {
            menuList.add(new MenuDef("Bezoekers", contextPath + "/bezoekers.html"));
        }

        if (webUserData.getCurrentlevel().ordinal() < 2) {
            menuList.add(new MenuDef("Berichten", contextPath + "/gebed.html"));
        }
        if (webUserData.getCurrentlevel().ordinal() < 1) {
            menuList.add(new MenuDef("Locaties", contextPath + "/locaties.html"));

        }
        menuList.add(new MenuDef("Instellingen", contextPath + "/settings.html"));
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public List<MenuDef> getMenuList() {
        return menuList;
    }

    public int getCurrentYear() {
        return currentYear;
    }

    public void setCurrentYear(int currentYear) {
        this.currentYear = currentYear;
    }

    public String getLogoutUrl() {
        return logoutUrl;
    }

    public void setLogoutUrl(String logoutUrl) {
        this.logoutUrl = logoutUrl;
    }


}
