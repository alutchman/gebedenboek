package com.crmweb.jsfbean;

import com.crmweb.services.LocatiesService;
import com.shared.transfers.LocatieData;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.component.datatable.DataTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@Slf4j
@Getter
@Setter
@ManagedBean
@ViewScoped
public class LocatieBean extends BaseJsfBean {
    private static final long serialVersionUID = -8703758340879573485L;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient LocatiesService locatiesService;

    private LocatieData newLocatie;

    private List<LocatieData> locaties;

    private DataTable locatieTable;

    private LocatieData currentLocatie;

    private LocatieData restoredCurrentLocatie;


    @Override
    protected void initialize() {
        if (!isForBeheer()) {
            return;
        }

        this.setActiveMenuDef("Locaties");

        locaties = findFromSession("LocatieBean::locaties");
        if (locaties == null) {
            resetLocaties();
        }
        this.setViewTitle("Locaties Aanpassen");
    }

    private void resetLocaties(){
        locaties = locatiesService.findAllLocations();
        saveToSession("LocatieBean::locaties", locaties);
    }

    public void cancelAddLocatie(){
        newLocatie = null;
    }


    public void addNewLocatie(){
        newLocatie = new LocatieData();
    }

    public void saveNewLocatie(){
        if (newLocatie.getNummer() <= 0) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( "Ongeldige huisnummer ingevuld.");
            facesContext.addMessage("ToevoegLocatie:Nummer", facesMessage);
            return;
        }

        LocatieData otherWithUkgrCode = locatiesService.findUkgrCodeLocatie(newLocatie.getUkgrcode(),0l);
        if (otherWithUkgrCode != null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( "Code UKGR"+ newLocatie.getUkgrcode() + " wordt gebruikt voor een andere locatie.");
            facesContext.addMessage("ToevoegLocatie:ukgrcode", facesMessage);
            return;
        }

        locatiesService.addNewLocatie(newLocatie);
        resetLocaties();
        newLocatie = null;
    }

    public void updateLocatie(){
        if (currentLocatie.getNummer() <= 0) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( "Ongeldige huisnummer ingevuld.");
            facesContext.addMessage("WijzigLocatie:Nummer", facesMessage);
            return;
        }

        LocatieData otherWithUkgrCode = locatiesService.findUkgrCodeLocatie(currentLocatie.getUkgrcode(), currentLocatie.getId());

        if (otherWithUkgrCode != null) {
            FacesContext facesContext = FacesContext.getCurrentInstance();
            FacesMessage facesMessage = new FacesMessage( "Code UKGR"+ currentLocatie.getUkgrcode() + " wordt gebruikt voor een andere locatie.");
            facesContext.addMessage("WijzigLocatie:ukgrcode", facesMessage);
            return;
        }

        locatiesService.updateLocatie(currentLocatie);
        resetLocaties();
        currentLocatie = null;
    }

    public void cancelUpdateLocatie(){
        currentLocatie = null;
    }


    public String gotoLocatie(){
        restoredCurrentLocatie = (LocatieData) getLocatieTable().getRowData();
        try {
            currentLocatie = restoredCurrentLocatie.clone();
        } catch (CloneNotSupportedException e) {

        }

        return null;
    }


}
