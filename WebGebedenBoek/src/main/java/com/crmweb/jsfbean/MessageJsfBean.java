package com.crmweb.jsfbean;


import com.crmweb.reports.PdfBytesProducer;
import com.crmweb.services.BezoekerService;
import com.crmweb.services.ImageService;
import com.crmweb.services.LocatiesService;
import com.crmweb.services.MessagesService;
import com.crmweb.utils.UserItemDataValues;
import com.crmweb.utils.ZipDataUsersUtils;
import com.shared.transfers.BezoekerData;
import com.shared.transfers.ImageDataTrf;
import com.shared.transfers.LocatieData;
import com.shared.transfers.MessageData;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.UploadedFileWrapper;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Slf4j
@Getter
@Setter
@ManagedBean
@ViewScoped
public class MessageJsfBean extends BaseJsfBean{
    private static final long serialVersionUID = -6003912966997471644L;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient PdfBytesProducer pdfBytesProducer;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient MessagesService messagesService;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient UploadedFileWrapper newMessagePlaatje;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient ImageService imageService;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient BezoekerService bezoekerService;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private transient LocatiesService locatiesService;

    private String textEdit;

    private String titleZoek;

    private Date invoerVanaf;

    private MessageData editMessage;

    private MessageData sendMessage;

    private MessageData newMessage;

    private MessageData locationMessage;

    private Long selectedSendLocation;

    private DataTable messageTable;

    private DataTable personTable;

    private String hintImage;

    private String koptekst = "Wij sturen u dit bericht vanuit de UKGR.";

    private String slotTekst = "Met vriendelijke groet \n\n";

    private List<MessageData> messageList;

    private String contentBase;

    private Set<BezoekerData> recipientInfoList;

    private List<BezoekerData>  searchedList;

    private List<LocatieData> searchLocatie;

    private DataTable reciptientTable;

    private BezoekerData searchRecipient ;

    private boolean disabledPdfPerson;

    private boolean disabledEmailPerson;


    private Long lastMillis = System.currentTimeMillis();

    @Override
    protected void initialize() {
        if (!isForAdmin()) {
            return;
        }
        this.setActiveMenuDef("Berichten");
        this.setViewTitle("Berichten");

        if (editMessage == null && newMessage == null) {
            if (titleZoek == null) {
                titleZoek = "%";
            }
            messageList = messagesService.findMessages(titleZoek, invoerVanaf);
        }

        HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().
                getExternalContext().getRequest();

        contentBase = String.format("%s/%s",getContextPath(), ImageDataTrf.BASE_CONTENT_NAME);


        Long sendID = findFromSession("MessageJsfBean::sendMessage");
        if (sendID != null) {
            sendMessage = messagesService.findMessageById(sendID);
        }

        Long sendIDLoc = findFromSession("MessageJsfBean::locationMessage");
        if (sendIDLoc != null) {
            locationMessage = messagesService.findMessageById(sendIDLoc);
        }

        searchRecipient = findFromSession("MessageJsfBean::searchRecipient");
        if (searchRecipient == null) {
            searchRecipient =  new BezoekerData();
        }

        Set<BezoekerData> oldRecipientList = findFromSession("MessageJsfBean::recipientInfoList");
        if (oldRecipientList != null) {
            recipientInfoList = oldRecipientList;
        } else {
            recipientInfoList = new HashSet<>();
        }

        searchedList = findFromSession("MessageJsfBean::searchedList");

        lastMillis = System.currentTimeMillis();


        slotTekst += getWebUserData().getVoornaam() +" " +  getWebUserData().getAchternaam() ;


        searchLocatie = locatiesService.findAllLocations();
        Long prevSelectedLocation = findFromSession("MessageJsfBean::selectedSendLocation");

        if (prevSelectedLocation != null) {
            selectedSendLocation = prevSelectedLocation;
        } else {
            selectedSendLocation = searchLocatie.size() > 0 ? searchLocatie.get(0).getId() : -1l;
        }


    }

    public void handleSearch(){
        messageList = messagesService.findMessages(titleZoek, invoerVanaf);
    }

    public void imageCalculator(FileUploadEvent event){
        if (event == null) {
            return;
        }

        long  max = 375 * 1024; // 375kB

        UploadedFile uploadedFileTmp = event.getFile();

        if (uploadedFileTmp.getSize() <= max){
            ImageDataTrf imageDataTrf = imageService.addNewMessageImage(uploadedFileTmp);
            //hintImage = imageDataTrf.findImageTag();
            String formattedName = imageDataTrf.findformattedName();
            HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
            //req.getLocalPort()
           // String imgSrc = String.format("https://%s%s/%s", req.getServerName(), getContextPath(), formattedName);
            //String imgSrc = String.format("http://localhost:%d%s/%s", req.getLocalPort(), getContextPath(), formattedName);
            String imgSrc = String.format("%s/%s", getContextPath(), formattedName);
            log.info("fullServer =  {}", imgSrc);
            String baseImageMem = "<img src=\"%s\"  width=\"150\" />";
            hintImage =  String.format(baseImageMem, imgSrc);
        } else {
            hintImage = String.format("Plaatje is te groot. U kunt max %dkB gebruiken. (Bij voorkeur jpg)", max/1024)  ;
        }
    }

    public void startAddMessage(){
        newMessage = new MessageData();
        newMessage.setTitle("Boodschap van Zegen....");
        newMessage.setMessagebody(messagesService.getHtmlNewMessage());
        hintImage = null;
    }

    public void saveAddMessage(){
        messagesService.saveNewMessage(newMessage);
        newMessagePlaatje = null;
        newMessage = null;
        messageList = messagesService.findMessages(titleZoek, invoerVanaf);
    }


    public void cancelAddMessage(){
        newMessagePlaatje = null;
        newMessage = null;
    }

    public void startEditMessage(){
        hintImage = null;

    }

    public void saveEditMessage(){
        try {
            messagesService.saveEditMessage(editMessage);
        } catch (Exception e) {
            log.error("Fout in saveEditMessage", e);
        }


        newMessagePlaatje = null;
        editMessage = null;
        messageList = messagesService.findMessages(titleZoek, invoerVanaf);
    }

    public void addPerson(BezoekerData person ) {
        if (person != null) {
            recipientInfoList.add(person);
        }
    }


    public void cancelEditMessage(){
        newMessagePlaatje = null;
        editMessage = null;
    }

    public void startPdfPrint(BezoekerData person){
        if (sendMessage == null || person == null || disabledPdfPerson ) {
            return;
        }

        disabledPdfPerson = true;
        try {
            MessageData pdfMessage = sendMessage.clone();

            pdfMessage.setKopTekst("<p>"+koptekst+"</p>");
            pdfMessage.setSlotTekst("<p>"+slotTekst+ "</p>");

            byte[] pdfBytes = pdfBytesProducer.convertHtmlToPdf(pdfMessage, person);
            if (pdfBytes == null) {
                return;
            }
            String filename = person.getFullName().replace(" ", "_")+ "." +
                    person.getPostcode() + ".pdf";
            FacesContext facesContext = FacesContext.getCurrentInstance();
            parseDownloadData(facesContext, pdfBytes, filename);
        } catch (CloneNotSupportedException e) {

        }
        disabledPdfPerson = false;
    }

    private void parseDownloadData(FacesContext facesContext, byte[] dataToSend, String filename){
        HttpServletResponse response =
                (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setHeader("Content-Description", "File Transfer");
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "0");
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-disposition", "attachment;filename="+filename);
        response.setHeader("Content-Length", String.valueOf(dataToSend.length));

        OutputStream responseOutputStream = null;
        try {
            responseOutputStream = response.getOutputStream();
            responseOutputStream.write(dataToSend, 0, dataToSend.length);
            responseOutputStream.flush();
            responseOutputStream.close();

            facesContext.responseComplete();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    public void startManualSend(){
        recipientInfoList = new HashSet<>();
        searchRecipient = new BezoekerData();
        saveToSession("MessageJsfBean::sendMessage",  sendMessage.getId());
    }

    public void startLocationSend(){
        saveToSession("MessageJsfBean::locationMessage",  locationMessage.getId());
        saveToSession("MessageJsfBean::selectedSendLocation",  selectedSendLocation);
    }

    public void endLocationSend(){
        locationMessage = null;

        removeFromSession("MessageJsfBean::locationMessage");
        removeFromSession("MessageJsfBean::selectedSendLocation");
    }
    public void handleLocationDownload(){
        LocatieData locatieData =   locatiesService.findLocatie(selectedSendLocation);
        log.info("ukgr code = {}", selectedSendLocation.toString());

        List<BezoekerData> bezoekers =   bezoekerService.findBezoeker(selectedSendLocation);

        log.info("aantal bezoekers = {}", bezoekers.size());
        List<UserItemDataValues> preparedZipData = new ArrayList<>();

        locationMessage.setKopTekst(koptekst);
        locationMessage.setSlotTekst(slotTekst);

        for (BezoekerData bezoekerData : bezoekers) {
            UserItemDataValues userItemDataValue = new UserItemDataValues();
            userItemDataValue.setBezoeker(bezoekerData);
            userItemDataValue.setPrayerDataPdf(pdfBytesProducer.convertHtmlToPdf(locationMessage,bezoekerData));
            preparedZipData.add(userItemDataValue);
        }

        String filename = locatieData.getZipFileName();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) facesContext.getExternalContext().getResponse();

        response.setContentType("application/force-download");
        response.setHeader("Content-Transfer-Encoding", "binary");
        response.setHeader("Content-Disposition", "attachment;filename="+ filename);

        try {
            ZipDataUsersUtils zipDataUsersUtils = new ZipDataUsersUtils();
            zipDataUsersUtils.compressZipData(response.getOutputStream(),preparedZipData);
        } catch (IOException e) {
           log.error(e.getMessage());
        } finally {
            facesContext.responseComplete();
            facesContext.renderResponse();
        }
    }

    public void changeLocation(AjaxBehaviorEvent event){
        log.info("ukgr code = "+ selectedSendLocation.toString());
        saveToSession("MessageJsfBean::selectedSendLocation",  selectedSendLocation);
    }

    public void endManualSend(){
        sendMessage = null;
        recipientInfoList = null;
        searchRecipient = new BezoekerData();
        searchedList = null;

        removeFromSession("MessageJsfBean::searchedList");
        removeFromSession("MessageJsfBean::sendMessage");
        removeFromSession("MessageJsfBean::recipientInfoList");
        removeFromSession("MessageJsfBean::searchRecipient");
    }

    public void handleManualSend(){
        if (recipientInfoList.size() == 0) {
            return;
        }
        sendAllEmailsWithSendMessage(recipientInfoList);
        endManualSend();
    }


    private void sendAllEmailsWithSendMessage(Set<BezoekerData> setOfVisitors){
        if (sendMessage == null || setOfVisitors == null || setOfVisitors.size() ==0) {
            return;
        }
        HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().
                getExternalContext().getRequest();

        sendMessage.setKopTekst(koptekst);
        sendMessage.setSlotTekst(slotTekst);

        messagesService.prepareMessageToSend(sendMessage,setOfVisitors);
        messagesService.executeMessageSend(sendMessage.getId(), request.getLocalPort());
    }

    public void removeRecipient(BezoekerData person){
        Long nextMillis = System.currentTimeMillis();
        Long delta = nextMillis - lastMillis;
        if (delta > 100) {
            lastMillis = nextMillis;
            if (sendMessage == null && person == null) {
                return;
            }
            recipientInfoList.remove(person);
            saveToSession("MessageJsfBean::recipientInfoList",  recipientInfoList);
        }
    }


    public void searchPerson(){
        if (searchRecipient == null ||   searchRecipient.getAchternaam() == null  ) {
            log.info("No Lastname supplied...");
            return;
        }
        searchedList = bezoekerService.findBezoeker( searchRecipient.getEmail(),searchRecipient.getVoornaam(), searchRecipient.getAchternaam());
        saveToSession("MessageJsfBean::searchRecipient",  searchRecipient);
        saveToSession("MessageJsfBean::searchedList",  searchedList);

    }

}
