package com.crmweb.jsfbean;


import com.crmweb.services.BezoekerService;
import com.crmweb.services.ImageService;
import com.crmweb.services.LocatiesService;
import com.shared.transfers.BezoekerData;
import com.shared.transfers.FileDataTrf;
import com.shared.transfers.LocatieData;
import lombok.extern.slf4j.Slf4j;
import org.primefaces.component.datatable.DataTable;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFileWrapper;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import java.io.IOException;
import java.util.List;

@Slf4j
@ManagedBean
@ViewScoped
public class BezoekersJsfBean  extends BaseJsfBean{
    private static final long serialVersionUID = -562100047014443941L;

    private String emailZoek;
    private String voornaamZoek;
    private String achternaamZoek;

    private transient BezoekerService bezoekerService;

    private transient LocatiesService locatiesService;

    private transient ImageService imageService;

    private List<BezoekerData> selectBezoeker;

    private List<LocatieData> locaties;

    private DataTable personTable;

    private BezoekerData currentBezoeker;

    private FileDataTrf currentUserImage;

    private BezoekerData newBezoeker;

    private transient UploadedFileWrapper newBezoekersImage;

    private String newEmail;

    private String text;


    @Override
    protected void initialize() {
        if (!isBezoekerForBeheer()) {
            return;
        }
        this.setActiveMenuDef("Bezoekers");
        this.setViewTitle("Bezoekers");

        emailZoek = this.findFromSession("BezoekersJsfBean::emailZoek");
        voornaamZoek = this.findFromSession("BezoekersJsfBean::voornaamZoek");
        achternaamZoek = this.findFromSession("BezoekersJsfBean::achternaamZoek");
        currentBezoeker = this.findFromSession("BezoekersJsfBean::currentBezoeker");
        newBezoeker = this.findFromSession("BezoekersJsfBean::newBezoeker");
        selectBezoeker = this.findFromSession("BezoekersJsfBean::selectBezoeker");
        if (currentBezoeker != null && currentBezoeker.getImageid() != null) {
            currentUserImage = imageService.getImageById(currentBezoeker.getImageid());
        }

        locaties = locatiesService.findAllLocations();
    }

    public void searchUser(){
        if ((emailZoek == null || emailZoek.trim().length() == 0) &&
            (voornaamZoek==null|| voornaamZoek.trim().length() == 0) &&
            (achternaamZoek == null|| achternaamZoek.trim().length() == 0)) {

            selectBezoeker = null;
            return;
        }

        saveToSession("BezoekersJsfBean::emailZoek",emailZoek.trim());
        saveToSession("BezoekersJsfBean::voornaamZoek",voornaamZoek.trim());
        saveToSession("BezoekersJsfBean::achternaamZoek",achternaamZoek.trim());

        List<BezoekerData> resultList = bezoekerService.findBezoeker(emailZoek, voornaamZoek, achternaamZoek);
        if (resultList.size() > 0) {
            selectBezoeker = resultList;
            saveToSession("BezoekersJsfBean::selectBezoeker",selectBezoeker);
        }
    }

    public void onRowSelect(SelectEvent event) {
        try {
            BezoekerData person = (BezoekerData) event.getObject();
            saveToSession("BezoekersJsfBean::currentBezoeker",person);

            FacesContext.getCurrentInstance().getExternalContext().redirect(this.getContextPath()+"/bezoekers.html");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addUser() {
        newBezoeker = new BezoekerData();
        newBezoeker.setLocatieId(1L);
        newBezoeker.setEmail(emailZoek);
        newBezoeker.setAchternaam(achternaamZoek);
        newBezoeker.setVoornaam(voornaamZoek);
        newBezoeker.setGeslacht("V");
        saveToSession("BezoekersJsfBean::newBezoeker",newBezoeker);
    }

    public void saveAddBezoeker() throws CloneNotSupportedException {
        bezoekerService.addBezoeker(newBezoeker,newBezoekersImage);
        newBezoeker = null;
        newBezoekersImage = null;
        this.removeFromSession("BezoekersJsfBean::newBezoeker");
    }

    public void deleteBezoeker(){
        bezoekerService.deleteBezoeker(currentBezoeker);
        emailZoek = null;
        currentBezoeker = null;
        currentUserImage = null;
        newEmail = null;
    }

    public void cancelAddBezoeker(){
        newBezoeker = null;
        newBezoekersImage = null;
        this.removeFromSession("BezoekersJsfBean::newBezoeker");
    }

    public void saveEditBezoeker(){
        bezoekerService.updateBezoeker(currentBezoeker,newEmail,newBezoekersImage);
        currentBezoeker = null;
        currentUserImage = null;
        newEmail = null;
        this.removeFromSession("BezoekersJsfBean::currentBezoeker");
    }


    public void cancelEditBezoeker(){
        newEmail = null;
        currentBezoeker = null;
        currentUserImage = null;
        this.removeFromSession("BezoekersJsfBean::currentBezoeker");
    }


    public String getEmailZoek() {
        return emailZoek;
    }


    public void setEmailZoek(String emailZoek) {
        this.emailZoek = emailZoek;
    }

    public BezoekerData getCurrentBezoeker() {
        return currentBezoeker;
    }

    public void setCurrentBezoeker(BezoekerData currentBezoeker) {
        this.currentBezoeker = currentBezoeker;
    }

    public BezoekerData getNewBezoeker() {
        return newBezoeker;
    }

    public void setNewBezoeker(BezoekerData newBezoeker) {
        this.newBezoeker = newBezoeker;
    }

    public UploadedFileWrapper getNewBezoekersImage() {
        return newBezoekersImage;
    }

    public void setNewBezoekersImage(UploadedFileWrapper newBezoekersImage) {
        this.newBezoekersImage = newBezoekersImage;
    }


    public FileDataTrf getCurrentUserImage() {
        return currentUserImage;
    }

    public void setCurrentUserImage(FileDataTrf currentUserImage) {
        this.currentUserImage = currentUserImage;
    }

    public String getNewEmail() {
        return newEmail;
    }

    public void setNewEmail(String newEmail) {
        this.newEmail = newEmail;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getVoornaamZoek() {
        return voornaamZoek;
    }

    public void setVoornaamZoek(String voornaamZoek) {
        this.voornaamZoek = voornaamZoek;
    }

    public String getAchternaamZoek() {
        return achternaamZoek;
    }

    public void setAchternaamZoek(String achternaamZoek) {
        this.achternaamZoek = achternaamZoek;
    }

    public List<BezoekerData> getSelectBezoeker() {
        return selectBezoeker;
    }

    public void setSelectBezoeker(List<BezoekerData> selectBezoeker) {
        this.selectBezoeker = selectBezoeker;
    }

    public DataTable getPersonTable() {
        return personTable;
    }

    public void setPersonTable(DataTable personTable) {
        this.personTable = personTable;
    }

    public List<LocatieData> getLocaties() {
        return locaties;
    }

    public void setLocaties(List<LocatieData> locaties) {
        this.locaties = locaties;
    }
}
