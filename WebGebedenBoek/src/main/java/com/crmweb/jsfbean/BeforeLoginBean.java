package com.crmweb.jsfbean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class BeforeLoginBean extends BaseJsfBean{

    private static final long serialVersionUID = 5629355238495650864L;

    @Override
    protected void initialize() {

    }
}
