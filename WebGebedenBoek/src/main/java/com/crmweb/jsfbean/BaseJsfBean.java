package com.crmweb.jsfbean;

import com.crmweb.services.UkgrProprtiesApplication;
import com.shared.transfers.UserLevel;
import com.shared.transfers.WebUserData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public abstract class BaseJsfBean implements Serializable,Cloneable {
    private static Map<Class,Object> wired = new HashMap<>();

    protected abstract void initialize();

    private WebUserData webUserData;

    private String contextPath;

    private String activeMenuDef;

    private String viewTitle;

    private transient UkgrProprtiesApplication settings;

    protected final <T> T readSpringBean(Class<T> aclass) {
        try {
            ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
            ServletContext servletContext = (ServletContext) externalContext.getContext();
            return (T) servletContext.getAttribute(aclass.getCanonicalName());
        } catch (Exception e) {
            log.error("error getting wired class: {}", e.getMessage());
            return null;
        }
    }

    @Override
    protected BaseJsfBean clone() throws CloneNotSupportedException {
        BaseJsfBean baseJsfBean = (BaseJsfBean) super.clone();
        baseJsfBean.setTransientMembers();
        return baseJsfBean;
    }

    protected void setTransientMembers(){
        Field[] flds = this.getClass().getDeclaredFields();
        for (Field field : flds) {
            boolean isPrivate  = Modifier.isPrivate(field.getModifiers());
            boolean isTransient = Modifier.isTransient(field.getModifiers());
            boolean isUsable = (field.getType().isAnnotationPresent(Service.class) ||
                    field.getType().isAnnotationPresent(Component.class));
            if (isPrivate && isTransient && isUsable) {
                field.setAccessible(true);
                try {
                    Object wiredService = readSpringBean(field.getType());
                    if (wiredService != null) {
                        field.set(this, wiredService);
                    }
                } catch (IllegalAccessException e) {

                }
                field.setAccessible(false);
            }
        }
    }

    private void readObject(ObjectInputStream in) throws IOException,ClassNotFoundException {
        in.defaultReadObject();
        this.setTransientMembers();
    }

    @PostConstruct
    public void init(){
        settings = readSpringBean(UkgrProprtiesApplication.class);
        contextPath = FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();

        webUserData = (WebUserData) FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get(WebUserData.class.getSimpleName());

        setTransientMembers();
        initialize();
    }

    protected boolean isForBeheer(){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();

        try {
            if (webUserData == null) {
                response.sendRedirect(this.getContextPath() + "/login.html");
                return false;
            } else if (getWebUserData().getCurrentlevel().ordinal() > UserLevel.BEHEER.ordinal()) {
                response.sendRedirect(this.getContextPath()+"/settings.html");
                return false;
            }
        } catch (IOException e) {

        }
        return true;
    }

    protected boolean isBezoekerForBeheer(){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();

        try {
            if (webUserData == null) {
                response.sendRedirect(this.getContextPath() + "/login.html");
                return false;
            } else if (getWebUserData().getCurrentlevel().ordinal() > UserLevel.BEZOEKERS.ordinal()) {
                response.sendRedirect(this.getContextPath()+"/settings.html");
                return false;
            }
        } catch (IOException e) {

        }
        return true;
    }

    protected boolean isForAdmin(){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse)context.getExternalContext().getResponse();

        try {
            if (webUserData == null) {
                response.sendRedirect(this.getContextPath() + "/login.html");
                return false;
            }else if (getWebUserData().getCurrentlevel().ordinal() > UserLevel.ADMIN.ordinal()) {
                response.sendRedirect(this.getContextPath()+"/settings.html");
                return false;
            }
        } catch (IOException e) {

        }
        return true;
    }

    public void removeFromSession(String item){
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().remove(retrieveSessionName(item));
    }

    public void saveToSession(String item, Object value){
        FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().put(retrieveSessionName(item), value);
    }

    public <T> T findFromSession(String item){
        T value = (T)  FacesContext.getCurrentInstance().
                getExternalContext().getSessionMap().get(retrieveSessionName(item));
        return value;
    }


    private String retrieveSessionName(String item){
        return String.format("%s:%s", this.getClass().getCanonicalName(), item);
    }

    public WebUserData getWebUserData() {
        return webUserData;
    }

    public void setWebUserData(WebUserData webUserData) {
        this.webUserData = webUserData;
    }

    public String getContextPath() {
        return contextPath;
    }

    public void setContextPath(String contextPath) {
        this.contextPath = contextPath;
    }

    public String getActiveMenuDef() {
        return activeMenuDef;
    }

    public void setActiveMenuDef(String activeMenuDef) {
        this.activeMenuDef = activeMenuDef;
    }

    public String getViewTitle() {
        return viewTitle;
    }

    public void setViewTitle(String viewTitle) {
        this.viewTitle = viewTitle;
    }

    public UkgrProprtiesApplication getSettings() {
        return settings;
    }
}
