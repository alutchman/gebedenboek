package com.crmweb.reports;

import com.crmweb.services.MessagesService;
import com.crmweb.services.UkgrProprtiesApplication;
import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.Pipeline;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.CssFilesImpl;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.HTML;
import com.itextpdf.tool.xml.html.TagProcessorFactory;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.shared.transfers.BezoekerData;
import com.shared.transfers.ImageDataTrf;
import com.shared.transfers.MessageData;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;

@Slf4j
@Component
public class PdfBytesProducer {
    @Autowired
    private UkgrProprtiesApplication ukgrProprtiesApplication;


    @Autowired
    private MessagesService messagesService;



    public byte[]  convertHtmlToPdf(MessageData messageData, BezoekerData person) {
        Map<String, ImageDataTrf > linkedData = messagesService.preProcessPdfData(messageData);

        String html =  messagesService.construnctMessageForPerson(person, messageData);
        html = html.replaceAll("<br>","<br />");
        html = html.replaceAll("<BR>","<br />");

        ByteArrayOutputStream outputStream = null;
        InputStream inputStream = null;
        byte[] bytes = null;
        try {

            outputStream = new ByteArrayOutputStream();

            float left = 40f;
            float right = 40f;
            float top = 50f;
            float bottom = 50f;
            Document document =  new Document(PageSize.A4, left, right, top, bottom);

            final PdfWriter writer = PdfWriter.getInstance(document, outputStream);
            document.open();

            // CSS

            InputStream csspathtest = Thread.currentThread()
                    .getContextClassLoader()
                    .getResourceAsStream("pdfoverzicht.css");
            CssFile cssfiletest = XMLWorkerHelper.getCSS(csspathtest);


            document.addTitle(messageData.getTitle());
            document.addCreationDate();
            document.addSubject(ukgrProprtiesApplication.getPdfTitle() + " voor "+ person.getFullName());
            document.addKeywords(ukgrProprtiesApplication.getPdfKeywords());
            document.addAuthor(ukgrProprtiesApplication.getPdfAuthor());
            document.addCreator(ukgrProprtiesApplication.getPdfAuthor());
            document.addLanguage("NL_nl");

            final TagProcessorFactory tagProcessorFactory = Tags.getHtmlTagProcessorFactory();
            tagProcessorFactory.removeProcessor(HTML.Tag.IMG);
            tagProcessorFactory.addProcessor(new ImageTagProcessor(linkedData), HTML.Tag.IMG);

            final CssFilesImpl cssFiles = new CssFilesImpl();
            cssFiles.add(XMLWorkerHelper.getInstance().getDefaultCSS());
            final StyleAttrCSSResolver cssResolver = new StyleAttrCSSResolver(cssFiles);
            cssResolver.addCss(cssfiletest);

            final HtmlPipelineContext hpc = new HtmlPipelineContext(new CssAppliersImpl(new XMLWorkerFontProvider()));
            hpc.setAcceptUnknown(true).autoBookmark(true).setTagFactory(tagProcessorFactory);
            final HtmlPipeline htmlPipeline = new HtmlPipeline(hpc, new PdfWriterPipeline(document, writer));
            final Pipeline<?> pipeline = new CssResolverPipeline(cssResolver, htmlPipeline);
            final XMLWorker worker = new XMLWorker(pipeline, true);
            final Charset charset = Charset.forName("UTF-8");
            final XMLParser xmlParser = new XMLParser(true, worker, charset);


            inputStream = new ByteArrayInputStream(html.getBytes(Charset.forName("UTF-8")));

            xmlParser.parse(inputStream);
            document.close();



        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }finally {
            try {
                if(null != outputStream) {
                    bytes = outputStream.toByteArray();
                    outputStream.close();
                }
                if(null != inputStream) {

                    inputStream.close();
                }
            } catch (IOException e) {

            }
            return bytes;
        }
    }


}
