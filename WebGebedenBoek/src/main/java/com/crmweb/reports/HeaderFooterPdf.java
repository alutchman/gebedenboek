package com.crmweb.reports;

import com.crmweb.services.UkgrProprtiesApplication;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;

import java.io.IOException;
import java.util.Map;

public class HeaderFooterPdf extends PdfPageEventHelper {

    private Font FONT_WM = new Font(Font.FontFamily.HELVETICA, 55, Font.BOLD, new GrayColor(0.98f));

    public static final String cssFooter = "<style>\n" +
            "    table.footer {\n" +
            "       font-size: 6pt !important;\n" +
            "       width : 100% !important; \n"+
            "       border-top: 1px solid red !important;\n" +
            "    }\n" +
            "    table.footer tr td {\n" +
            "       font-size: 6pt !important;\n" +
            "        padding-top: 2px;\n" +
            "        font-family: Arial;\n" +
            "    }\n" +
            "\n" +
            "</style>\n" ;



    public static final String FOOTER_HTML= cssFooter+ "<table class=\"footer\"   >\n" +
            "        <tr>\n" +
            "            <td style=\"width:8cm !important;\"> #KVKSTUFF </td>\n" +
            "            <td style=\"text-align:center;\">- #CURRENTPAGE/#TOTALPAGES -</td>\n" +
            "            <td style=\"text-align:right;width:6cm !important;\"> #IDENT </td>\n" +
            "        </tr>\n" +
            "    </table>";


    private int totalPages=1;

    private  Map<String, String> params;

    private UkgrProprtiesApplication ukgrProprtiesApplication;

    public HeaderFooterPdf(int totalPages, UkgrProprtiesApplication ukgrProprtiesApplication, Map<String, String> params) throws IOException {
        super();
        this.totalPages = totalPages;
        this.ukgrProprtiesApplication = ukgrProprtiesApplication;
        this.params = params;
    }

    //onStartPage onEndPage
    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        printFooter(writer, document);
    }


    private void printFooter(PdfWriter writer, Document document) {
        try {
            final int currentPageNumber = writer.getCurrentPageNumber();

            ColumnText.showTextAligned(writer.getDirectContentUnder(),
                    Element.ALIGN_CENTER, new Phrase(ukgrProprtiesApplication.getWatermark(), FONT_WM),
                    277.5f, 400,  -55);
            String footer2Use = FOOTER_HTML.
                    replaceAll("#KVKSTUFF", ukgrProprtiesApplication.getPageFooter()).
                    replaceAll("#CURRENTPAGE", String.valueOf(currentPageNumber)).
                    replaceAll("#TOTALPAGES", String.valueOf(totalPages)).
                    replaceAll("#IDENT", ukgrProprtiesApplication.getDirector());
            ElementList footer = XMLWorkerHelper.parseToElementList(footer2Use, cssFooter);



            ColumnText ct = new ColumnText(writer.getDirectContent());
            Rectangle pageRect = document.getPageSize();

            Rectangle footRect = new Rectangle(pageRect.getLeft()+100f, pageRect.getBottom()+60,
                            pageRect.getRight()-40f, pageRect.getBottom()+10);



            footRect.setTop(pageRect.getBottom() - document.bottomMargin());
            footRect.setLeft(pageRect.getLeft() + document.leftMargin());
            footRect.setRight(pageRect.getRight()-document.rightMargin());

            ct.setSimpleColumn(footRect);
            //ct.setSimpleColumn(new Rectangle(36, 50, 559, 40));
            for (Element e : footer) {
                ct.addElement(e);
            }
            ct.go();

        } catch (Exception de) {


        }
    }
}
