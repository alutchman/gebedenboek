package com.crmweb.reports;


import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public final class MiscCalculations {

    /**
     * Block for singleton
     */
    private MiscCalculations(){

    }


    public static String getBaseUrlFromRequest(HttpServletRequest request){
        return "http://127.0.0.1:8080/"+request.getContextPath();
    }

    /**
     *
     * @param uri
     * @param params
     * @param sessionID
     * @return
     */
    public static ResponseEntity<String> postMultivalueMapToUrl(String uri, MultiValueMap<String, String> params, String sessionID){
        RestTemplate restTemplate = new RestTemplate();

        HttpMessageConverter<?> formHttpMessageConverter = new FormHttpMessageConverter();
        HttpMessageConverter<?> stringHttpMessageConverter = new StringHttpMessageConverter();
        List<HttpMessageConverter> msgConverters = new ArrayList<HttpMessageConverter>();
        msgConverters.add(formHttpMessageConverter);
        msgConverters.add(stringHttpMessageConverter);

        // Prepare acceptable media type
        List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
        acceptableMediaTypes.add(MediaType.ALL);

        // Prepare header
        HttpHeaders headers = new HttpHeaders();

        //===IMPORTANT=====
        headers.add("Cookie", sessionID);
        headers.setAccept(acceptableMediaTypes);
        HttpEntity<MultiValueMap<String,String>> httpEntity = new HttpEntity<>(params,headers);
        ResponseEntity<String> result = restTemplate.postForEntity(uri, httpEntity, String.class);

        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {

        }
        return result;
    }


    /**
     *
     * @param uri
     * @param params
     * @param sessionId
     * @return
     */
    public static ResponseEntity<String> postHashMapToUrl(String uri, Map<String, String> params, String sessionId){
        MultiValueMap<String, String> paramsMultiValue = new LinkedMultiValueMap<>();
        paramsMultiValue.setAll(params);

        return postMultivalueMapToUrl(uri, paramsMultiValue, sessionId);
    }
}
