# README #

* Multi Module
* Spring Web MVC 5.0.2
* Hibernate 5.2
* JPA
* JSF 2.2.15 Java Server Faces
* Annotation driven design.
* Spring Unit Test using H2 in memory database
* Logback used for logging

# tomcat context.xml addition required #
	  <Resource name="jdbc/ukgrDB" 
	    auth="Container" 
	    type="javax.sql.DataSource"
        maxActive="100" maxIdle="30" maxWait="10000"
        username="XXXXX" password="XXXXX" 
        driverClassName="com.mysql.jdbc.Driver"
        url="jdbc:mysql://127.0.0.1:3306/ukgradmin"/>

# web.xml addition required #
    <?xml version="1.0" encoding="UTF-8"?>
    <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
             xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
             xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
                                 http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd" version="3.1">
        <display-name>Ukgr Offerande Administratie</display-name>
        <resource-ref>
            <description>DB Connection</description>
            <res-ref-name>jdbc/ukgrDB</res-ref-name>
            <res-type>javax.sql.DataSource</res-type>
            <res-auth>Container</res-auth>
        </resource-ref>